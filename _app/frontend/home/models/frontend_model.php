<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Frontend_model extends CI_Model {

	//sets validation rules for new email
  public $validation_userLogin = array(
    			array(
    			  'field' => 'usernameoremail',
    			  'label' => 'email',
    			  'rules' => 'required|trim|sanitize'
    			),
    			array(
    			  'field' => 'password',
    			  'label' => 'email type',
    			  'rules' => 'required|trim|sanitize'
    			),
  );

  //sets validation rules for new user
  public $validation_newUser = array(
            array(
              'field' => 'firstname',
              'label' => 'firstname',
              'rules' => 'required|trim|sanitize'
            ),
            array(
              'field' => 'lastname',
              'label' => 'lastname',
              'rules' => 'required|trim|sanitize'
            ),
            array(
              'field' => 'street',
              'label' => 'street',
              'rules' => 'required|trim|sanitize'
            ),
            array(
              'field' => 'fulladdress',
              'label' => 'fulladdress',
              'rules' => 'required|trim|sanitize'
            ),
            array(
              'field' => 'postcode',
              'label' => 'postcode',
              'rules' => 'required|trim|sanitize'
            ),
            array(
              'field' => 'mobileno',
              'label' => 'mobileno',
              'rules' => 'required|trim|sanitize|min_length[10]|max_length[10]'
            ),
            array(
              'field' => 'othertel',
              'label' => 'othertel',
              'rules' => 'trim|sanitize|min_length[9]'
            ),
            array(
              'field' => 'email',
              'label' => 'email',
              'rules' => 'required|trim|sanitize|valid_email|is_unique[user.email]'
            ),
            array(
              'field' => 'username',
              'label' => 'username',
              'rules' => 'required|trim|sanitize|min_length[5]|max_length[25]|is_unique[user.login_username]'
            ),
            array(
              'field' => 'password',
              'label' => 'password',
              'rules' => 'required|trim|sanitize|min_length[6]|max_length[15]'
            ),
            array(
              'field' => 'repassword',
              'label' => 're-password',
              'rules' => 'required|trim|sanitizemin_length[6]|max_length[15]|matches[password]'
            )
  );

  /**
   * function to validate......
   * @return 
   */
  public function validateUser($usernameoremail, $hashpsw){
  	$query = $this->db->query("SELECT * FROM user WHERE ( login_username = '$usernameoremail' OR email = '$usernameoremail') AND login_password = '$hashpsw'");
		if($query->num_rows()==1)
			return $query->result(); //return result
		else
			return false;
  }

  /**
   *
   * @return 
   */
  public function getUserDetailsById($user_id){
    $query = $this->db->get_where('user', array('user_id' => $user_id));
    return $query->result();
  }

 /**
   *
   * @return 
   */
  public function insertUser($insertData){
    $this->db->insert('user',$insertData);
    return $this->db->insert_id();
  }


  public function totalRecentDeals(){
    $query = $this->db->select('*')
                      ->where('item.item_status', 'unsold')
                      ->from('item')
                      ->count_all_results();
    return $query;  }

  /**
   *
   * @return 
   */
  public function getRecentDeals($offset){
    $sessUserId = $this->session->userdata('userid');
    $whereArr = array(
      'item.item_status' => 'unsold',
      'item.user_id !=' => $sessUserId,
      'auction.auction_status' => 'open'
    );
    $query = $this->db->select('*')
                      ->where($whereArr)
                      ->from('item')
                      ->join('category', "item.category_id = category.category_id")
                      ->join('product', "item.product_id = product.product_id")
                      ->join('auction', "auction.item_id = item.item_id")
                      ->order_by('item_timestamp', 'DESC')
                      ->limit(4, $offset)
                      ->get();
    return $query->result();
  }

  /**
   *
   * @return 
   */
  public function getOtherDeals(){
    $sessUserId = $this->session->userdata('userid');
    $whereArr = array(
      'item.item_status' => 'unsold',
      'item.user_id !=' => $sessUserId,
      'auction.auction_status' => 'open'
    );
    $query = $this->db->select('*')
                      ->where($whereArr)
                      ->from('item')
                      ->join('category', "item.category_id = category.category_id")
                      ->join('product', "item.product_id = product.product_id")
                      ->join('auction', "auction.item_id = item.item_id")
                      ->order_by('item_id', 'RANDOM')
                      ->limit(4)
                      ->get();
    return $query->result();
  }

  /**
   *
   * @return 
   */
  public function getItesmsByProduct($productName){
    $sessUserId = $this->session->userdata('userid');
    $whereArr = array(
      'item.item_status' => 'unsold',
      'item.user_id !=' => $sessUserId,
      'auction.auction_status' => 'open',
      'product.product_name' => $productName
    );
    $query = $this->db->select('*')
                      ->where($whereArr)
                      ->from('item')
                      ->join('category', "item.category_id = category.category_id")
                      ->join('product', "item.product_id = product.product_id")
                      ->join('auction', "auction.item_id = item.item_id")
                      ->order_by('item_timestamp', 'DESC')
                      ->get();
    return $query->result();
  }

  /**
   *
   * @return 
   */
  public function insertBid($insertData){
    $query = $this->db->insert('bid', $insertData);
    if($query){
      return true;
    }
  }

  /**
   *
   * @return 
   */
  public function insertPurchase($insertData){
    $query = $this->db->insert('purchase', $insertData);
    if($query){
      return true;
    }
  }

  /**
   *
   * @return 
   */
  public function updateSPrice($itemId, $startPrice){
    $data = array('start_price' => $startPrice);
    $query = $this->db->where('item_id', $itemId)
            ->update('auction', $data);
    if($query){
      return true;
    }
  }

  /**
   *
   * @return 
   */
  public function updateItemStatus($item_id){
    $data = array('item_status' => 'sold');
  }

  /**
   *
   * @return 
   */
  public function validateMOffer($item_id, $user_id){
    $data = array(
      'item_id' => $item_id,
      'user_id' => $user_id
    );
    $query = $this->db->get_where('purchase', $data);
    if($query->num_rows() == 0)
      return true;
    else
      return false;
  }

  /**
   *
   * @return 
   */
  public function getBidEmails($item_id, $user_id){
    $data = array(
      'bid.item_id' => $item_id,
      'user.user_id !=' => $user_id
    );
    $query = $this->db->select('user.email')
                      ->where($data)
                      ->distinct()
                      ->from('bid')
                      ->join('user','bid.user_id = user.user_id','left')
                      ->get();
    return $query->result('array');
  }

  /**
   *
   * @return 
   */
  public function getOpenAuctions(){
    $query = $this->db->get_where('auction', array('auction_status' => 'open'));
    return $query->result();
  }

  /**
   *
   * @return 
   */
  public function updateAuctionStatus($auction_id){
    $this->db->where('auction_id', $auction_id);
    $this->db->update('auction',array('auction_status' => 'closed'));
  }

  /**
   *
   * @return 
   */
  public function updateItemStatusUnsold($item_id){
    $this->db->where('item_id', $item_id);
    $this->db->update('item',array('item_status' => 'unsold'));
  }

  /**
   *
   * @return 
   */
  public function getItemsBySearch($searchKey){
    $sessUserId = $this->session->userdata('userid');
    $whereArr = array(
      'item.item_status' => 'unsold',
      'item.user_id !=' => $sessUserId,
      'auction.auction_status' => 'open'
    );
    $query1 = $this->db->select('*')
                      ->where($whereArr)
                      // ->like('product.product_name', $searchKey)
                      ->like('item.item_name', $searchKey)
                      ->from('item')
                      ->join('category', "item.category_id = category.category_id")
                      ->join('product', "item.product_id = product.product_id")
                      ->join('auction', "auction.item_id = item.item_id")
                      ->order_by('item_timestamp', 'DESC')
                      ->limit(16)
                      ->get();
    if($query1->result() == null){
      $query2 = $this->db->select('*')
                      ->where($whereArr)
                      ->like('product.product_name', $searchKey)
                      // ->like('item.item_name', $searchKey)
                      ->from('item')
                      ->join('category', "item.category_id = category.category_id")
                      ->join('product', "item.product_id = product.product_id")
                      ->join('auction', "auction.item_id = item.item_id")
                      ->order_by('item_timestamp', 'DESC')
                      ->limit(16)
                      ->get();
      return $query2->result();
    } else {
    return $query1->result();
    }
  }

  /**
   *
   * @return 
   */
  public function maxBid($item_id){
    $query = $this->db->select('*')
                      ->select_max('bid_price')
                      ->where('bid.item_id', $item_id)
                      ->from('bid')
                      ->join('user', 'bid.user_id = user.user_id','left')
                      ->join('item', 'bid.item_id = item.item_id','left')
                      ->get();
    return $query->result();
  }

}

/* End of file frontend_model.php */
/* Location: .//D/xampp/htdocs/electro/_app/frontend/home/models/frontend_model.php */