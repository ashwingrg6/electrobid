<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MX_Controller {

	/******************** constructor function **********************/
	public function __construct()
	{
		parent::__construct();
		$this->load->model('frontend_model','frontend');
	}

	/**
	 *
	 * @return
	 */
	public function index(){
		$this->checkAuction();
		$totalRecentDeals = $this->frontend->totalRecentDeals();
		$data['recentdeals1'] = $this->frontend->getRecentDeals(0);
		$cntFlag1 = count($data['recentdeals1']);
		if($cntFlag1 == 4){
			$cntFlag2 = count($this->frontend->getRecentDeals(4));
			if($cntFlag2 > 0){
				$data['recentdeals2'] = $this->frontend->getRecentDeals(4);
				if($cntFlag2 == 4){
					$cntFlag3 = count($this->frontend->getRecentDeals(8));
					if($cntFlag3 > 0){
						$data['recentdeals3'] = $this->frontend->getRecentDeals(8);
						if($cntFlag3 == 4){
							$cntFlag4 = count($this->frontend->getRecentDeals(12));
							if($cntFlag4 > 0){
								$data['recentdeals4'] = $this->frontend->getRecentDeals(12);
							}
						}
					}
				}
			}
		}
		$data['otherdeals'] = $this->frontend->getOtherDeals();
		/*$data['users'] = array(
			'user_id' => '1',
			'name' => 'ashwingrg'
				);
		$data['some_data'] = 'some data here';*/
		$user_id = $this->session->userdata('userid');
		$data['allcategories'] = Modules::run('category/returnAllCategories');
		$data ['allproducts'] = Modules::run('product/returnAllProducts');
		$data['user_details'] = $this->frontend->getUserDetailsById($user_id);
		$this->template->load('frontend/template/main', 'homepage',$data);

	}

	/**
	 *
	 * @return 
	 */
	public function login(){
		if($_POST):
			$usernameoremail = $this->input->post("usernameoremail", TRUE);
			$password = $this->input->post("password", TRUE);
			$hashpsw = hash("sha512", $password);
      $this->form_validation->set_rules($this->frontend->validation_userLogin);
			if($this->form_validation->run() == TRUE):
				$userFlag = $this->frontend->validateUser($usernameoremail,$hashpsw);
				if($userFlag):
					$userdata = array(
						'userinfo' => $userFlag,
						'userid' => $userFlag[0]->user_id,
						'usertype' => $userFlag[0]->user_type,
						'loggedInUser' => TRUE
					);
					$this->session->set_userdata($userdata);
					$msg = array(
						'loginSuccess' => '<b style="color:blue;"">Loading.....</b>'
					);
					echo json_encode($msg);
				else:
					$msg = array(
					'loginError' => 'Invalid username/email or password! Both fields are required.<br>Please try again.'
					);
					echo json_encode($msg);
				endif;
			else:
				$msg = array(
					'loginError' => 'Invalid username/email or password! Both fields are required.<br>Please try again.'
				);
				echo json_encode($msg);
			endif; //validation if ends
		else:
			redirect('home');
		endif; //parent if, if posted
	}

	/**
	 *
	 * @return 
	 */
	public function signup(){
		if(!$this->session->userdata('userid')):
			if($_POST):
				$this->form_validation->set_rules($this->frontend->validation_newUser);
	    	if($this->form_validation->run() == TRUE):
	    		$firstname = $this->input->post('firstname', TRUE);
					$lastname = $this->input->post('lastname', TRUE);
					$street = $this->input->post('street', TRUE);
					$fulladdress = $this->input->post('fulladdress', TRUE);
					$postcode = $this->input->post('postcode', TRUE);
					$mobileno = $this->input->post('mobileno', TRUE);
					$otherno = $this->input->post('othertel', TRUE);
					$email = $this->input->post('email', TRUE);
					$username = $this->input->post('username', TRUE);
					$password = $this->input->post('password', TRUE);
					$hashpsw = hash("sha512",$password);
					$currentdate = $this->returnCurrentDate();
					$insertData = array(
						'first_name' => $firstname,
						'last_name' => $lastname,
						'street' => $street,
						'address' => $fulladdress,
						'postcode' => $postcode,
						'mobile_no' => $mobileno,
						'other_no' => $otherno,
						'email' => $email,
						'login_username' => $username,
						'user_type' => 'user',
						'login_password' => $hashpsw,
						'date_of_join' => $currentdate
					);
					$insertFlag = $this->frontend->insertUser($insertData);
					if($insertFlag){
						$this->load->library('email');
						$this->email->from('admin@admin.com', 'Super Admin');
						$this->email->to($email);
						$this->email->subject('subject');
						$this->email->message('You have successfully registered.....');
						$this->email->send();
						// echo $this->email->print_debugger();
						$this->session->set_flashdata('userAdded', 'New user has been registered successfully.');
						redirect('home/signup');
					};
	    	else:
	        $this->form_validation->set_error_delimiters('','');
					$user_id = $this->session->userdata('userid');
					$data['user_details'] = $this->frontend->getUserDetailsById($user_id);
					$this->template->load('frontend/template/main', 'signup_view',$data);
	    	endif;//validation if ends
			else:
				$user_id = $this->session->userdata('userid');
				$data['user_details'] = $this->frontend->getUserDetailsById($user_id);
				$this->template->load('frontend/template/main', 'signup_view',$data);
			endif;//if ends, form post
		else:
			redirect('home');
		endif;
	}

	/**
	 *
	 * @return 
	 */
	public function returnCurrentDate(){
		$time = now();
    $dateformat = "%Y-%m-%d";
    return mdate($dateformat, $time);
	}

	/**
	 *
	 * @return 
	 */
	public function items($productName){
		$itemsFlag = count($this->frontend->getItesmsByProduct($productName));
		if($itemsFlag != null){
			$data['filtereditems'] = $this->frontend->getItesmsByProduct($productName);
		}
		$user_id = $this->session->userdata('userid');
		$data['otherdeals'] = $this->frontend->getOtherDeals();
		$data['allcategories'] = Modules::run('category/returnAllCategories');
		$data ['allproducts'] = Modules::run('product/returnAllProducts');
		$data['user_details'] = $this->frontend->getUserDetailsById($user_id);
		$this->template->load('frontend/template/main', 'item_filter_view',$data);
	}

	/**
	 *
	 * @return 
	 */
	public function bid(){
		if($_POST){
			$item_id = $this->input->post('itemid', TRUE);
			$user_id = $this->input->post('userid', TRUE);
			$bid_price = $this->input->post('bid_price', TRUE);
			$bid_start_price = $this->input->post('bid_start_price', TRUE);
			$ntime = now();
			$currentdate = $this->returnCurrentDate();
			if($bid_price <= $bid_start_price){
				// $this->session->set_flashdata('lessBidPrice', 'Please enter the price more than mentioned bid start price.');
				$msg = array(
					'priceError' => 'Please enter the price more than above mentioned bid start price.'
				);
				echo json_encode($msg);
			}
			else{
				$insertData = array(
					'item_id' => $item_id,
					'user_id' => $user_id,
					'bid_price' => $bid_price,
					'bid_date' => $currentdate,
					'bid_timestamp' => $ntime
				);
				$insertFlag = $this->frontend->insertBid($insertData);
				if($insertFlag){
					$emails = $this->frontend->getBidEmails($item_id, $user_id);
					$this->load->library('email');
					$this->email->from('admin@admin.com', 'Super Admin');
					// $this->email->to('alsdkjfa');
					@$this->email->to($emails);
					$this->email->subject('New Bid');
					$this->email->message('message');
					$this->email->send();

					// echo $this->email->print_debugger();
					$startPrice = $bid_price;
					$updateSPriceFlag = $this->frontend->updateSPrice($item_id, $startPrice);
					if($updateSPriceFlag){
						$this->session->set_flashdata('bidInserted', 'Success: You have bid the selected item.');
						// redirect('home');
						$msg = array();
						echo json_encode($msg);
					}
				}
			}
		}
	}

	/**
	 *
	 * @return 
	 */
	public function offer(){
		$item_id = $this->input->post('itemid', TRUE);
		$user_id = $this->input->post('userid', TRUE);
		$item_price = $this->input->post('itemPrice', TRUE);
		$currentdate = $this->returnCurrentDate();
		$ntime = now();
		$validateOfferFlag = $this->frontend->validateMOffer($item_id, $user_id);
		if($validateOfferFlag){
			$insertData = array(
				'item_id' => $item_id,
				'user_id' => $user_id,
				'purchase_date' => $currentdate,
				'purchase_timestamp' => $ntime
			);
			$insertFlag = $this->frontend->insertPurchase($insertData);
			if($insertFlag){
				$itemSFlag = $this->frontend->updateItemStatus($item_id);
				$this->session->set_flashdata('offerInserted', 'Success: You have made an offer for the selected item.');
				session_start();
				$_SESSION["item_price"] = $item_price;
				redirect(URL.'paypal');
			}
		}
		else{
			$this->session->set_flashdata('errorOffer', 'Error: You have already made an offer for the selected item.');
			redirect('home');
		}
	}

	/**
	 *
	 * @return 
	 */
	public function checkAuction(){
		$auctions = $this->frontend->getOpenAuctions();
		foreach ($auctions as $key => $value) {
			$date = $value->close_date;
			$time = now();
		  $dateformat = "%Y-%m-%d";
		  $cDate = mdate($dateformat, $time);
		  if($cDate > $date){
		  	$auction_id = $value->auction_id;
		  	$bidWinner = $this->frontend->maxBid($auction_id);
		  	$this->load->library('email');

		  	$this->email->from('admin@admin.com', 'Super Admin');
		  	$this->email->to($bidWinner[0]->email);

		  	$this->email->subject('Bid Winner');
		  	$this->email->message('message.....');

		  	$this->email->send();
		  	// echo $this->email->print_debugger();

		  	$this->frontend->updateAuctionStatus($auction_id);
		  	$this->frontend->updateItemStatusUnsold($auction_id);
		  }
		}
	}

	/**
	 *
	 * @return 
	 */
	public function search(){
		$searchKey = $this->input->post('searchKey', TRUE);
		$data['searchedItems'] = $this->frontend->getItemsBySearch($searchKey);
		$user_id = $this->session->userdata('userid');
		$data['allcategories'] = Modules::run('category/returnAllCategories');
		$data ['allproducts'] = Modules::run('product/returnAllProducts');
		$data['user_details'] = $this->frontend->getUserDetailsById($user_id);
		$this->template->load('frontend/template/main', 'homepage_search_view',$data);
	}

}


/* End of file electro.php */
/* Location: ./application/frontend/electro/controllers/electro.php */