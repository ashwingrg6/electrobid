<?php
  $sessUserId = $this->session->userdata('userid');
?>
<!-- CategoriesDropdown -->
<div class="row">
  <div class="col-md-3">
  <h3 style="margin:0px; padding:0px; margin-left:8%;">Sign up - <em><span style="font-size:80%">Register new user</span></em></h3>
    
  </div>
  <div class="col-md-6 pull-right">
              <form class="form" role="search">
                <div class="form-group" style="display:inline;" action="<?php echo URL.'home/search'; ?>" method="post">
                  <div class="input-group"> 
                    <input type="text" class="form-control" placeholder="I'm looking for....." name="searchKey">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-search"></span> </span>
                  </div>
                </div>
              </form>

  </div>
</div>

</nav>

<!--End ofSearchBar-->
 
            
<!------------------------------------- **** End of categories dropdown****---------------------------------------------->
           
   <!------------ Container-fluid------------------------------------->
       
<!--------------------------------------------End of Top Fixed Nav Bar--------------------------------------------------->



<!-- Recent Deal Thumbnail Starts-->
<div id="index_body" style="background-color:#fff; padding-top:5px; padding-bottom:1em; margin-top:7em;">
  <div class="container-fluid" >
     <div class="row" style="margin-left:1%;">
     <?php if($this->session->flashdata('userAdded')){ ?>
      <!--notify msg-->
      <div class="alert alert-success alert-dismissable" style="padding:8px; border-radius:0px; width:66%;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo $this->session->flashdata('userAdded'); ?>
      </div><!--msg notify ends-->
      <?php } ?>
      <!--Form starts here-->
      <form action="<?php echo site_url('home/signup'); ?>" method="post" role="form">
        <!--First name & last name-->
        <div class="row">
          <div class="form-group col-lg-3">
            <label for="firstname">
              First Name :
              <small>&nbsp;&nbsp;eg. john</small>
            </label>
            <input type="text" name="firstname" id="firstname" value="<?php echo set_value('firstname');?>" class="form-control" style="border-radius:0px;" placeholder="First Name">
            <span class="error">
              <?php echo form_error('firstname'); ?>
            </span>
          </div><!--first name ends-->
          <div class="form-group col-lg-3">
            <label for="lastname">
              Last Name :
              <small>&nbsp;&nbsp;eg. smith</small>
            </label>
            <input type="text" name="lastname" id="lastname" value="<?php echo set_value('lastname');?>" class="form-control" style="border-radius:0px;" placeholder="Last Name">
            <span class="error">
              <?php echo form_error('lastname'); ?>
            </span>
          </div>
        </div>
        <!--first name and last name end here-->      
        <!--street and full address start-->      
        <div class="row">
          <div class="form-group col-lg-3">
            <label for="street">
              Street :
              <small>&nbsp;&nbsp;eg. lazimpat</small>
            </label>
            <input type="text" name="street" id="street" value="<?php echo set_value('street');?>" class="form-control" style="border-radius:0px;" placeholder="Street">
            <span class="error">
              <?php echo form_error('street'); ?>
            </span>
          </div>
          <div class="form-group col-lg-5">
            <label for="fulladdress">
              Full Address :
              <small>&nbsp;&nbsp;eg. Lazimpat-2, Kathmandu, Nepal</small>
            </label>
            <input type="text" name="fulladdress" id="fulladdress" value="<?php echo set_value('fulladdress');?>" class="form-control" style="border-radius:0px;" placeholder="Full Address">
            <span class="error">
              <?php echo form_error('fulladdress'); ?>
            </span>
          </div>
        </div>
        <!--street and full address end here-->      
        <!--mobile no. and other no. start-->      
        <div class="row">
          <div class="form-group col-lg-2">
            <label for="postcode">
              Post Code:
              <small>&nbsp;&nbsp;eg. 977</small>
            </label>
            <input type="number" name="postcode" id="postcode" value="<?php echo set_value('postcode');?>" class="form-control" style="border-radius:0px;" placeholder="Post Code">
            <span class="error">
              <?php echo form_error('postcode'); ?>
            </span>
            </div>
          <div class="form-group col-lg-3">
            <label for="mobileno">
              Mobile No.:
              <small>&nbsp;&nbsp;eg. 980000000</small>
            </label>
            <input type="number" name="mobileno" id="mobileno" value="<?php echo set_value('mobileno');?>" class="form-control" style="border-radius:0px;" placeholder="Mobile No.">
            <span class="error">
              <?php echo form_error('mobileno'); ?>
            </span>
          </div>
          <div class="form-group col-lg-3">
            <label for="othertel">
              Other Tel No.:
              <small>&nbsp;&nbsp;eg. 014441561</small>
            </label>
            <input type="number" name="othertel" id="othertel" value="<?php echo set_value('othertel');?>" class="form-control" style="border-radius:0px;" placeholder="Other Tel No.">
            <span class="error">
              <?php echo form_error('othertel'); ?>
            </span>
          </div>
        </div>
        <!--mobile no. and other no. end here-->      
        <!--email start-->      
        <div class="row">
          <div class="form-group col-lg-4">
            <label for="email">
              Email :
              <small>&nbsp;&nbsp;eg. gloomy.gurung@gmail.com</small>
            </label>
            <input type="text" name="email" id="email" value="<?php echo set_value('email');?>" class="form-control" style="border-radius:0px;" placeholder="Email">
            <span class="error">
              <?php echo form_error('email'); ?>
            </span>
          </div>
        </div>
        <!--email ends here-->      
        <!--username and usertype start-->      
        <div class="row">
          <div class="form-group col-lg-3">
            <label for="username">Login username:
              <small>&nbsp;&nbsp;eg. john56</small>
            </label>
            <input type="text" name="username" id="username" value="<?php echo set_value('username'); ?>" class="form-control" style="border-radius:0px;" placeholder="Login username">
            <span class="error">
              <?php echo form_error('username'); ?>
            </span>
          </div><!--login username ends here-->
        </div>
        <!--username ends here-->
        <!--password-->
        <div class="row">
          <div class="form-group col-lg-3">
            <label for="password">
              Login Password :
              <small>&nbsp;&nbsp;eg. p@ssword</small>
            </label>
            <input type="password" name="password" id="password" value="<?php echo set_value('password'); ?>" class="form-control" style="border-radius:0px;" placeholder="**********">
            <span class="error">
              <?php echo form_error('password'); ?>
            </span>
          </div>
          <div class="form-group col-lg-3">
            <label for="repassword">
              Confirm Password :
            </label>
            <input type="password" name="repassword" id="repassword" value="<?php echo set_value('repassword'); ?>" class="form-control" style="border-radius:0px;" placeholder="***********">
            <span class="error">
              <?php echo form_error('repassword'); ?>
            </span>
          </div>
        </div><!--password ends here-->
        <!--submit and cancel button-->
        <!-- <input type="submit" value="SUBMIT"> -->
        <div class="row">
          <p>
            <button type="submit" class="btn btn-primary" style="margin-left:1em; border-radius:0px;">Sign up</button>
            <button type="reset" onclick="window.location.href='<?=site_url('home'); ?>'" class="btn btn-danger" style="margin-top:0px; border-radius:0px;">Cancel</button>
          </p><hr>
        </div>

        <!--submit and cancel button end here-->
      </form>
      <!--Form ends here--> 

     </div>
   </div>
 </div>



<style>
  .loginError{
    color: red;
  }

</style>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <!--login form starts-->
      <form action="" method="post" name="loginForm" id="loginForm">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Member Log-in</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <span class="loginError"></span>
            </div>
            <div class="form-group">
          <label for="exampleInputEmail1">Username or Email address</label>&nbsp;&nbsp;
          <input class="form-control" id="exampleInputEmail1" placeholder="Enter username or email" type="text" name="usernameoremail">
          </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Password</label>
        <input class="form-control" id="exampleInputPassword1" placeholder="Password" type="password" name="password">
        </div>
            <p class="text-right"><a href="#">Forgot password?</a></p>
          </div>
          <div class="modal-footer">
            <a href="#" data-dismiss="modal" class="btn">Close</a>
            <button class="btn btn-primary" type="submit">Log-in</button>
          </div>
        </div>
      </form><!--login form ends here-->
    </div>
</div>
