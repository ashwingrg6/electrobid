<?php
  $sessUserId = $this->session->userdata('userid');
?>
<style>
  .btnText { color: #fff; }
  .btnText:hover { color: #fff; }
</style>
<!-- CategoriesDropdown -->
            <div class="nav navbar">
            <div class="row">
                 <ul class=" nav nav-pills active pull-left"  role="tablist">
                      <li class="dropdown mega-dropdown">
                        <a class="dropdown-toggle"  data-toggle="dropdown" href="#" style="font-family: Tahoma, Geneva, sans-serif ; color: #2B2B2B"> <strong>CATEGORIES</strong>
                          <span class=" glyphicon glyphicon-chevron-down"></span>
                        </a>
                        <ul class="dropdown-menu mega-dropdown-menu row" role="menu"aria-labelledby="dropdownMenu2">
                        <?php foreach ($allcategories as $key => $value): ?>
                          <li class="col-sm-6">
                            <ul>
                              <li role="presentation" class="dropdown-header"><?php echo $value->category_name;?></li>
                              <?php foreach ($allproducts as $key => $value2): 
                                if($value->category_id == $value2->category_id){
                              ?>
                              <li role="presentation">
                                <a role="menuitem" tabindex="-1" href="<?php echo site_url('home/items').'/'.$value2->product_name; ?>"><?php echo $value2->product_name; ?></a>
                              </li>
                            <?php }; endforeach; ?>
                            </ul>
                          </li>
                        <?php endforeach; ?>
                        </ul>
                      </li>
                 </ul>
             <div class=" col-md-5 col-md-offset-3 container pull-right">
              <form class="form" role="search" action="<?php echo URL.'home/search'; ?>" method="post">
                <div class="form-group" style="display:inline;">
                  <div class="input-group"> 
                    <input type="text" class="form-control" placeholder="I'm looking for....." name="searchKey">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-search"></span> </span>
                  </div>
                </div>
              </form>
             </div>
           </div>
           </div>
           </nav>

<!--End ofSearchBar-->
 
            
<!------------------------------------- **** End of categories dropdown****---------------------------------------------->
           
   <!------------ Container-fluid------------------------------------->
       
<!--------------------------------------------End of Top Fixed Nav Bar--------------------------------------------------->

<!-- Recent Deal Thumbnail Starts-->
<div id="index_body">
  <div class="container-fluid" >
     <div class="row">
       <div  class="panel-heading" >
         <ul class="nav nav-tabs" role="tablist">
           <li class="active">
              <a href="">Recent Deals</a>
           </li>
            <?php if($this->session->flashdata('bidInserted')){ ?>
            <li style="padding-top:3px;" class="col-md-6">
              <!--notify msg-->
              <div class="alert alert-success alert-dismissable" style="padding:8px; border-radius:0px;">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('bidInserted'); ?>
              </div><!--msg notify ends-->
            </li>
            <?php } ?>
            <?php if($this->session->flashdata('offerInserted')){ ?>
            <li style="padding-top:3px;" class="col-md-6">
              <!--notify msg-->
              <div class="alert alert-success alert-dismissable" style="padding:8px; border-radius:0px;">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('offerInserted'); ?>
              </div><!--msg notify ends-->
            </li>
            <?php } ?>
            <?php if($this->session->flashdata('errorOffer')){ ?>
            <li style="padding-top:3px;" class="col-md-6">
              <!--notify msg-->
              <div class="alert alert-danger alert-dismissable" style="padding:8px; border-radius:0px;">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('errorOffer'); ?>
              </div><!--msg notify ends-->
            </li>
            <?php } ?>
         </ul>
       </div>
       <div id="myCarousel" class="carousel slide">
         <div class="well-none">
           <!-- Carousel items -->     
            <div class="carousel-inner">
            <div class="item active">
               <div class="row"><?php // print_r($recentdeals1);die(); ?>
                <?php 
                  $snFlag1 = 1;
                  foreach ($recentdeals1 as $key => $value): 
                ?>
                  <div class="col-sm-3 col-xs-12">
                    <div class="thumbnail">
                      <img data- src="<?php echo URL1.'/uploads/'.$value->item_image;?>     
                     " alt=" ">
                      <div class="caption">
                        <h3><?php echo $value->item_name; ?></h3>
                        <p>Price: Rs. <?php echo $value->item_price; ?>
                        <?php if($sessUserId == null){ ?>
                        <span>&nbsp;&nbsp;<a href="" data-toggle="modal" data-target="#<?php echo 'myModal'.$snFlag1; ?>">View details</a></span>
                        <?php } ?>
                        </p>
                        <p>
                          <form action="<?php echo site_url('home/offer'); ?>" method="post">
                          <input type="hidden" name="itemid" value="<?php echo $value->item_id; ?>">
                          <input type="hidden" name="itemPrice" value="<?php echo $value->item_price; ?>">
                          <input type="hidden" name="userid" value="<?php echo $this->session->userdata('userid'); ?>">
                          <button class="btn btn-primary" <?php if($this->session->userdata('userid')==null){ echo "disabled"; } ?> type="submit">Make an offer</button>
                        <a href="" class="btn btn-default <?php if($this->session->userdata('userid')==null){ echo 'disabled'; } ?>" data-toggle="modal" data-target="#<?php echo 'myModal'.$snFlag1; ?>">Bid this item</a>
                          </form>
                       </p>
                     </div>
                   </div>
                 </div>
                 <!-- Modal1 -->
                 <div class="modal fade" id="myModal<?php echo $snFlag1; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                     <div class="modal-content">
                       <div class="modal-header">
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                           <span aria-hidden="true">&times;</span>
                         </button>
                         <h4 class="modal-title" id="myModalLabel"><?php echo $value->item_name; ?></h4>
                       </div>
                       <div class="modal-body">
                          <div class="row" style="padding:0px; margin:0px;"><?php echo $value->item_description; ?></div>
                          <div class="row" style="padding:0px; margin:0px;"><strong>Bid Start Price:</strong> <?php echo $value->start_price; ?></div><hr>
                          <?php if($sessUserId != NULL){ ?>
                          <div class="row" >
                                <div class="row bidError" style="margin-left:1em; padding:0px; color:red;"></div>
                            <form action="<?php echo site_url('home/bid'); ?>" method="post" role="form" name="bidForm" class="bidForm">
                              <div class="form-group col-md-6">
                                <input type="hidden" name="itemid" value="<?php echo $value->item_id; ?>">
                                <input type="hidden" name="userid" value="<?php echo $this->session->userdata('userid'); ?>">
                                <input type="hidden" name="bid_start_price" value="<?php echo $value->start_price; ?>">
                                <label for="price">Enter Price:</label>
                                <input type="number" name="bid_price" id="bid_price" class="form-control"placeholder="Enter price above <?php echo $value->start_price; ?>">
                              </div><div class="clearfix"></div>
                              <div class="form-group" style="margin-left:1em;">
                                <button class="btn btn-primary" type="submit">BID</button>
                              </div>
                            </form>
                          </div>
                          <?php } ?>
                       </div>
                       <div class="modal-footer">
                         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                       </div>
                     </div>
                   </div>
                 </div><!--Modal ends-->
                <?php $snFlag1++; endforeach; ?>
               </div>
               <!--/row-->
            </div>
            <!--recentdeals2-->
            <?php if(isset($recentdeals2)){ ?>
            <div class="item">
              <div class="row">
                <?php 
                  $snFlag2 = 5;
                  foreach ($recentdeals2 as $key => $value): 
                ?>
                  <div class="col-sm-3 col-xs-12">
                    <div class="thumbnail">
                      <img data- src="<?php echo URL1.'/uploads/'.$value->item_image;?>     
                     " alt=" ">
                      <div class="caption">
                        <h3><?php echo $value->item_name; ?></h3>
                        <p>Price: Rs. <?php echo $value->item_price; ?>
                        <?php if($sessUserId == null){ ?>
                        <span>&nbsp;&nbsp;<a href="" data-toggle="modal" data-target="#<?php echo 'myModal'.$snFlag2; ?>">View details</a></span>
                        <?php } ?>
                        </p>
                        <p>
                          <form action="<?php echo site_url('home/offer'); ?>" method="post">
                          <input type="hidden" name="itemid" value="<?php echo $value->item_id; ?>">
                          <input type="hidden" name="itemPrice" value="<?php echo $value->item_price; ?>">
                          <input type="hidden" name="userid" value="<?php echo $this->session->userdata('userid'); ?>">
                          <button class="btn btn-primary" <?php if($this->session->userdata('userid')==null){ echo "disabled"; } ?> type="submit">Make an offer</button>
                        <a href="" class="btn btn-default <?php if($this->session->userdata('userid')==null){ echo 'disabled'; } ?>" data-toggle="modal" data-target="#<?php echo 'myModal'.$snFlag2; ?>">Bid this item</a>
                          </form>
                       </p>
                     </div>
                   </div>
                 </div>
                 <!-- Modal -->
                 <div class="modal fade" id="myModal<?php echo $snFlag2; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                     <div class="modal-content">
                       <div class="modal-header">
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                           <span aria-hidden="true">&times;</span>
                         </button>
                         <h4 class="modal-title" id="myModalLabel"><?php echo $value->item_name; ?></h4>
                       </div>
                       <div class="modal-body">
                        <div class="row" style="padding:0px; margin:0px;"><?php echo $value->item_description; ?></div>
                          <div class="row" style="padding:0px; margin:0px;"><strong>Bid Start Price:</strong> <?php echo $value->start_price; ?></div><hr>
                          <?php if($sessUserId != NULL){ ?>
                          <div class="row" >
                            <form action="<?php echo site_url('home/bid'); ?>" method="post" role="form" name="bidForm" class="bidForm">
                              <div class="form-group col-md-6">
                                <input type="hidden" name="itemid" value="<?php echo $value->item_id; ?>">
                                <input type="hidden" name="userid" value="<?php echo $this->session->userdata('userid'); ?>">
                                <input type="hidden" name="bid_start_price" value="<?php echo $value->start_price; ?>">
                                <label for="price">Enter Price:</label>
                                <input type="number" name="bid_price" id="bid_price" class="form-control"placeholder="Enter price above <?php echo $value->start_price; ?>">
                              </div><div class="clearfix"></div>
                              <div class="form-group" style="margin-left:1em;">
                                <button class="btn btn-primary" type="submit">BID</button>
                              </div>
                            </form>
                          </div>
                          <?php } ?>
                       </div>
                       <div class="modal-footer">
                         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                       </div>
                     </div>
                   </div>
                 </div><!--Modal ends-->
                <?php $snFlag2; endforeach; ?>
              </div>
            </div>
            <?php } ?>
            <!--recentdeals2 ends-->
            <!--recentdeals3-->
            <?php if(isset($recentdeals3)){ ?>
            <div class="item">
              <div class="row">
                <?php 
                  $snFlag3 = 9;
                  foreach ($recentdeals3 as $key => $value): 
                ?>
                  <div class="col-sm-3 col-xs-12">
                    <div class="thumbnail">
                      <img data- src="<?php echo URL1.'/uploads/'.$value->item_image;?>     
                     " alt=" ">
                      <div class="caption">
                        <h3><?php echo $value->item_name; ?></h3>
                        <p>Price: Rs. <?php echo $value->item_price; ?>
                        <?php if($sessUserId == null){ ?>
                        <span>&nbsp;&nbsp;<a href="" data-toggle="modal" data-target="#<?php echo 'myModal'.$snFlag3; ?>">View details</a></span>
                        <?php } ?>
                        </p>
                        <p>
                          <form action="<?php echo site_url('home/offer'); ?>" method="post">
                          <input type="hidden" name="itemid" value="<?php echo $value->item_id; ?>">
                          <input type="hidden" name="itemPrice" value="<?php echo $value->item_price; ?>">
                          <input type="hidden" name="userid" value="<?php echo $this->session->userdata('userid'); ?>">
                          <button class="btn btn-primary" <?php if($this->session->userdata('userid')==null){ echo "disabled"; } ?> type="submit">Make an offer</button>
                        <a href="" class="btn btn-default <?php if($this->session->userdata('userid')==null){ echo 'disabled'; } ?>" data-toggle="modal" data-target="#<?php echo 'myModal'.$snFlag3; ?>">Bid this item</a>
                          </form>
                       </p>
                     </div>
                   </div>
                 </div>
                 <!-- Modal -->
                 <div class="modal fade" id="myModal<?php echo $snFlag3; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                     <div class="modal-content">
                       <div class="modal-header">
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                           <span aria-hidden="true">&times;</span>
                         </button>
                         <h4 class="modal-title" id="myModalLabel"><?php echo $value->item_name; ?></h4>
                       </div>
                       <div class="modal-body">
                         <div class="row" style="padding:0px; margin:0px;"><?php echo $value->item_description; ?></div>
                          <div class="row" style="padding:0px; margin:0px;"><strong>Bid Start Price:</strong> <?php echo $value->start_price; ?></div><hr>
                          <?php if($sessUserId != NULL){ ?>
                          <div class="row" >
                            <form action="<?php echo site_url('home/bid'); ?>" method="post" role="form" name="bidForm" class="bidForm">
                              <div class="form-group col-md-6">
                                <input type="hidden" name="itemid" value="<?php echo $value->item_id; ?>">
                                <input type="hidden" name="userid" value="<?php echo $this->session->userdata('userid'); ?>">
                                <input type="hidden" name="bid_start_price" value="<?php echo $value->start_price; ?>">
                                <label for="price">Enter Price:</label>
                                <input type="number" name="bid_price" id="bid_price" class="form-control"placeholder="Enter price above <?php echo $value->start_price; ?>">
                              </div><div class="clearfix"></div>
                              <div class="form-group" style="margin-left:1em;">
                                <button class="btn btn-primary" type="submit">BID</button>
                              </div>
                            </form>
                          </div>
                          <?php } ?>
                       </div>
                       <div class="modal-footer">
                         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                       </div>
                     </div>
                   </div>
                 </div><!--Modal ends-->
                <?php $snFlag3++; endforeach; ?>
              </div>
            </div>
            <?php } ?>
            <!--recentdeals3 ends-->
            <!--recentdeals4-->
            <?php if(isset($recentdeals3)){ ?>
            <div class="item">
              <div class="row">
                <?php 
                  $snFlag4 = 13;
                  foreach ($recentdeals4 as $key => $value): 
                ?>
                  <div class="col-sm-3 col-xs-12">
                    <div class="thumbnail">
                      <img data- src="<?php echo URL1.'/uploads/'.$value->item_image;?>     
                     " alt=" ">
                      <div class="caption">
                        <h3><?php echo $value->item_name; ?></h3>
                        <p>Price: Rs. <?php echo $value->item_price; ?>
                        <?php if($sessUserId == null){ ?>
                        <span>&nbsp;&nbsp;<a href="" data-toggle="modal" data-target="#<?php echo 'myModal'.$snFlag4; ?>">View details</a></span>
                        <?php } ?>
                        </p>
                        <p>
                          <form action="<?php echo site_url('home/offer'); ?>" method="post">
                          <input type="hidden" name="itemid" value="<?php echo $value->item_id; ?>">
                          <input type="hidden" name="itemPrice" value="<?php echo $value->item_price; ?>">
                          <input type="hidden" name="userid" value="<?php echo $this->session->userdata('userid'); ?>">
                          <button class="btn btn-primary" <?php if($this->session->userdata('userid')==null){ echo "disabled"; } ?> type="submit">Make an offer</button>
                        <a href="" class="btn btn-default <?php if($this->session->userdata('userid')==null){ echo 'disabled'; } ?>" data-toggle="modal" data-target="#<?php echo 'myModal'.$snFlag4; ?>">Bid this item</a>
                          </form>
                       </p>
                     </div>
                   </div>
                 </div>
                 <!-- Modal -->
                 <div class="modal fade" id="myModal<?php echo $snFlag4; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                     <div class="modal-content">
                       <div class="modal-header">
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                           <span aria-hidden="true">&times;</span>
                         </button>
                         <h4 class="modal-title" id="myModalLabel"><?php echo $value->item_name; ?></h4>
                       </div>
                       <div class="modal-body">
                         <div class="row" style="padding:0px; margin:0px;"><?php echo $value->item_description; ?></div>
                          <div class="row" style="padding:0px; margin:0px;"><strong>Bid Start Price:</strong> <?php echo $value->start_price; ?></div><hr>
                          <?php if($sessUserId != NULL){ ?>
                          <div class="row" >
                            <form action="<?php echo site_url('home/bid'); ?>" method="post" role="form" name="bidForm" class="bidForm">
                              <div class="form-group col-md-6">
                                <input type="hidden" name="itemid" value="<?php echo $value->item_id; ?>">
                                <input type="hidden" name="userid" value="<?php echo $this->session->userdata('userid'); ?>">
                                <input type="hidden" name="bid_start_price" value="<?php echo $value->start_price; ?>">
                                <label for="price">Enter Price:</label>
                                <input type="number" name="bid_price" id="bid_price" class="form-control"placeholder="Enter price above <?php echo $value->start_price; ?>">
                              </div><div class="clearfix"></div>
                              <div class="form-group" style="margin-left:1em;">
                                <button class="btn btn-primary" type="submit">BID</button>
                              </div>
                            </form>
                          </div>
                          <?php } ?>
                       </div>
                       <div class="modal-footer">
                         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                       </div>
                     </div>
                   </div>
                 </div><!--Modal ends-->
                <?php $snFlag4++; endforeach; ?>
              </div>
            </div>
            <?php } ?>
            <!--recentdeals4 ends-->
          </div><!--/carousel-inner-->

<!--Carousel Control-->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev"><i class=" glyphicon glyphicon-chevron-left"></i></a>
	<a class="right carousel-control" href="#myCarousel" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
   </div> <!--/myCarousel-->
  </div>
 </div>
<hr />

 <!-- Other Deal Thumbnail Starts--> 
 <div class="row">
   <div  class=" panel-heading">
     <ul class="nav nav-tabs" role="tablist">
       <li class="active">
         <a href="#">
           Featured deals
           <span class="caret"></span>
         </a>
       </li>
     </ul>
   </div>
   <div id="myCarousel" class="carousel slide">
     <div class="well-none">
       <!-- Carousel items --> 
       <div class="carousel-inner">
        <!--other deals-->
         <div class="item active">
           <div class="row">
            <?php 
                  $sn = 1;
                  foreach ($otherdeals as $key => $value): 
                ?>
                  <div class="col-sm-3 col-xs-12">
                    <div class="thumbnail">
                      <img data- src="<?php echo URL1.'/uploads/'.$value->item_image;?>     
                     " alt=" ">
                      <div class="caption">
                        <h3><?php echo $value->item_name; ?></h3>
                        <p>Price: Rs. <?php echo $value->item_price; ?>
                        <?php if($sessUserId == null){ ?>
                        <span>&nbsp;&nbsp;<a href="" data-toggle="modal" data-target="#<?php echo 'myModall'.$sn; ?>">View details</a></span>
                        <?php } ?>
                        </p>
                        <p>
                          <form action="<?php echo site_url('home/offer'); ?>" method="post">
                          <input type="hidden" name="itemid" value="<?php echo $value->item_id; ?>">
                          <input type="hidden" name="itemPrice" value="<?php echo $value->item_price; ?>">
                          <input type="hidden" name="userid" value="<?php echo $this->session->userdata('userid'); ?>">
                          <button class="btn btn-primary" <?php if($this->session->userdata('userid')==null){ echo "disabled"; } ?> type="submit">Make an offer</button>
                        <a href="" class="btn btn-default <?php if($this->session->userdata('userid')==null){ echo 'disabled'; } ?>" data-toggle="modal" data-target="#<?php echo 'myModal'.$sn; ?>">Bid this item</a>
                          </form>
                       </p>
                     </div>
                   </div>
                 </div>
                 <!-- Modal1 -->
                 <div class="modal fade" id="myModall<?php echo $sn; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                     <div class="modal-content">
                       <div class="modal-header">
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                           <span aria-hidden="true">&times;</span>
                         </button>
                         <h4 class="modal-title" id="myModalLabel"><?php echo $value->item_name; ?></h4>
                       </div>
                       <div class="modal-body">
                          <div class="row" style="padding:0px; margin:0px;"><?php echo $value->item_description; ?></div>
                          <div class="row" style="padding:0px; margin:0px;"><strong>Bid Start Price:</strong> <?php echo $value->start_price; ?></div><hr>
                          <?php if($sessUserId != NULL){ ?>
                          <div class="row" >
                            <form action="<?php echo site_url('home/bid'); ?>" method="post" role="form" name="bidForm" class="bidForm">
                              <div class="form-group col-md-6">
                                <input type="hidden" name="itemid" value="<?php echo $value->item_id; ?>">
                                <input type="hidden" name="userid" value="<?php echo $this->session->userdata('userid'); ?>">
                                <input type="hidden" name="bid_start_price" value="<?php echo $value->start_price; ?>">
                                <label for="price">Enter Price:</label>
                                <input type="number" name="bid_price" id="bid_price" class="form-control"placeholder="Enter price above <?php echo $value->start_price; ?>">
                              </div><div class="clearfix"></div>
                              <div class="form-group" style="margin-left:1em;">
                                <button class="btn btn-primary" type="submit">BID</button>
                              </div>
                            </form>
                          </div>
                          <?php } ?>
                       </div>
                       <div class="modal-footer">
                         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                       </div>
                     </div>
                   </div>
                 </div><!--Modal ends-->
                <?php $sn++; endforeach; ?>
           <!--/row--> </div>
         <!--/item--> 
       <!--/carousel-inner--> 
       <!--Carousel Control--> 
       <!-- <a class="left carousel-control" href="#myCarousel" data-slide="prev"> <i class=" glyphicon glyphicon-chevron-left"></i>
       </a>
       <a class="right carousel-control" href="#myCarousel" data-slide="next"> <i class="glyphicon glyphicon-chevron-right" style="color: #000;"></i>
       </a> -->
     </div>
     <!--/myCarousel--> </div>
 </div>
 </div>
  <hr />

<style>
  .loginError{
    color: red;
  }

</style>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
      <!--login form starts-->
      <form action="" method="post" name="loginForm" id="loginForm">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Member Log-in</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <span class="loginError"></span>
            </div>
            <div class="form-group">
      		<label for="exampleInputEmail1">Username or Email address</label>&nbsp;&nbsp;
      		<input class="form-control" id="exampleInputEmail1" placeholder="Enter username or email" type="text" name="usernameoremail">
    		  </div>
  		  <div class="form-group">
  		  	<label for="exampleInputPassword1">Password</label>
  			<input class="form-control" id="exampleInputPassword1" placeholder="Password" type="password" name="password">
  		  </div>
            <p class="text-right"><a href="#">Forgot password?</a></p>
          </div>
          <div class="modal-footer">
            <a href="#" data-dismiss="modal" class="btn">Close</a>
            <button class="btn btn-primary" type="submit">Log-in</button>
          </div>
        </div>
      </form><!--login form ends here-->
    </div>
</div>

