<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1"  />
<title>Electro Bid</title>
<link rel="stylesheet" href="<?=FRONTEND.'css/bootstrap.min.css'?>">
<link rel="stylesheet" href="<?=FRONTEND.'css/style.css'?>">
<script src="<?=FRONTEND.'js/respond.js'?>"></script>
<script src="<?=FRONTEND.'js/bootstrap.min.js'?>"></script>
<script src="<?=FRONTEND.'js/jquery-1.11.0.min.js'?>"></script>

<style type="text/css">
    .error {
      color: red;
      font-style: italic;
      font-size: 12px;
    }
    label {
      font-weight: normal;
    }
</style>

</head>
<body>
<!-- Top Fixed NavBar-->
<?php
  $sessUserId = $this->session->userdata('userid');
?>
     <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <a class="navbar-brand" href="<?php echo site_url('home'); ?>" style="color: #000; font-family:Tahoma, Geneva, sans-serif;" > <span class='glyphicon glyphicon-home collapsed-element'></span> LOGO</a>
        <div class="btn-group btn-breadcrumb pull-right ">
          <?php if($this->session->flashdata('loggedOut')){  ?>
          <span class="loggedOutMsg" style="color:maroon; text-align:center; font-size:13px;"><em><?php echo $this->session->flashdata('loggedOut'); ?></em></span>
          <?php } ?>
          <?php if($sessUserId){ ?>
            <a  href="<?=site_url('dashboard'); ?>" class="btn btn-default"><strong>C-PANEL </strong></br><small>Switch to C-Panel</small></a>
            <a  href="" class="btn btn-default">Active:<strong><?php echo $user_details[0]->first_name;echo ' '.$user_details[0]->last_name; ?></strong></br><small>Email:&nbsp;<?php echo $user_details[0]->email; ?></small></a>
            <a href="<?=site_url('users/logout'); ?>" class="btn btn-default"><strong>Log out</strong></br><small>Click to logout.</small></a>
          <?php } else{ ?>
            <a  href="#myModal" data-toggle="modal" data-target="#myModal" class="btn btn-default"><strong>LOGIN </strong></br><small>Access your account</small></a>
            <a href="<?=site_url('home/signup'); ?>" class="btn btn-default"><strong>SIGN UP</strong></br><small>Become a FREE member</small></a>
            <a href="#" class="btn btn-default"><strong>HELP</strong></br><small>Need some help?.</small></a>
          <?php } ?>
        </div>

  </div>