
<!-----------------------------------------------------Footer ------------------------------------------------>
 <div class="navbar navbar-default navbar-fixed-bottom">
     <div class="container">
      <p class="navbar-text pull-left">© 2014 - electroBid
      </p>
        <!--<a href="http://tinyurl.com/tbvalid" target="_blank" >xxxx</a>
      </p>

      <a href="" class="navbar-btn btn-danger btn pull-right">
      <span class="glyphicon glyphicon-star"></span>  Subscribe on YouTube</a> -->
     </div>    
   </div>
<!----------------------------------------------------End of Footer------------------------------------------>
<script src="<?=FRONTEND.'js/bootstrap.min.js'?>"></script>
<script src="<?=FRONTEND.'js/jquery-1.11.0.min.js'?>"></script>

<script>
	$(document).ready(function(){
    $('form#loginForm').on('submit',function(e){
      e.preventDefault();
      formData = $(this).serialize();
      // console.log(formData);
      var output = '';
      var loginFlag = '';
      $.ajax({
        url : '<?php echo URL; ?>home/login',
        type : 'POST',
        data : formData,
        dataType: 'json',
        success : function(response){
          $.each(response,function(key,val){
            if(key == 'loginSuccess'){
              loginFlag = 'loggedin';
            }
            output += val;
          });
          $('span.loginError').html(output);
          if(loginFlag == 'loggedin'){
            window.location.href='<?php echo site_url('dashboard'); ?>';
          }
        }
      })
    });

    $('form.bidForm').on('submit',function(e){
      e.preventDefault();
      formData = $(this).serialize();
      var output = '';
      var biderror = '';
      $.ajax({
        url : "<?php echo URL; ?>home/bid",
        type : 'POST',
        data : formData,
        dataType : 'json',
        success : function(response){
          $.each(response,function(key,val){
            if(key == 'priceError'){
              biderror = 'yes';
            }
            output += val;
          });
          if(biderror == 'yes'){
            $('.bidError').html(output);
            alert(output);
            // console.log(output);
            // debugger;
          }
          else{
            window.location.href = '<?php echo current_url(); ?>';
          }
        }
      })
    });

    setTimeout(fade_out, 2500);
    function fade_out() {
      $("span.loggedOutMsg").fadeOut().empty();
    }
  });

</script>
</body>
</html>

