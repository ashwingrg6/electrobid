<?php
  $usertype = $userdetails[0]->user_type;
 ?>

<!-------------------------------------------Sidebar------------------------------------------------------------>
<div id="wrapper" class="active">
  <nav class='sidebar sidebar-menu-collapsed'>
    <a href='#' id='justify-icon'>
      <span class='glyphicon glyphicon-align-justify'></span>
    </a>
    <ul>
      <?php if($usertype != 'admin'){ ?>
      <li class="dropdown-submenu ">
        <a class='expandable caret-right' href='#' title='Dashboard' id="test">
          <span class='glyphicon glyphicon-home '></span>
          <span class='expanded-element' style=" font-size:18px">Items</span>
        </a>
        <ul class="dropdown-menu" id="test2">
          <li>
            <a href="<?php echo site_url('item/add'); ?>">
              Add item
            </a>
          </li>
          <li class="divider"></li>
          <li>
            <a href="<?php echo site_url('item'); ?>">
              View all items
            </a>
          </li>
        </ul>
      </li>
      <?php } ?>
      <?php if($usertype == 'admin'){ ?>
      <li class="dropdown-submenu">
        <a class='expandable caret-right' href='#' title='Dashboard' id="userMenu">
          <span class='glyphicon glyphicon-user collapsed-element'></span>
          <span class='expanded-element' style=" font-size:18px">Users</span>
        </a>
        <ul class="dropdown-menu" id="userSubmenu" >
          <li>
            <a href="<?=site_url('users/add'); ?>">
              <!-- <span class="glyphicon glyphicon-plus"></span> -->
              Add new admin
            </a>
          </li>
          <li>
            <a href="<?=site_url('users'); ?>">
              <!-- <span class="glyphicon glyphicon-eye-open"></span> -->
              List all users
            </a>
          </li>
        </ul>
      </li>
      <!--Category-SubCategory-->
      <li class="dropdown-submenu">
        <a class='expandable caret-right' href='#' title='Dashboard' id="categoryMenu">
          <span class='glyphicon glyphicon-cog collapsed-element'></span>
          <span class='expanded-element' style=" font-size:18px"></span>
        </a>
        <ul class="dropdown-menu" id="categorySubmenu" >
          <li>
            <a href="<?php echo site_url('category'); ?>">
              Manage Categories
            </a>
          </li>
          <li>
            <a href="<?php echo site_url('product'); ?>">
              Manage Products
            </a>
          </li>
        </ul>
      </li><!--category-subcategory ends-->
      <?php } ?>
    </ul>
  </nav>
</div><!--wrapper ends-->

<!-------------------------------------------------Page Content------------------------------------------------------------>
<div class="page-content-wrapper" style="width:92%; margin-top:4em; margin-left:6em; padding-bottom: 5em;">
  <div class="page-content inset">
    <!-- Keep all page content within the page-content inset div! -->
    <div class="row" id="section1">
      <div class="col-md-12">

