<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1"  />
  <title>C-Panel System, Electrobid</title>
  <link rel="stylesheet" href="<?=FRONTEND.'css/bootstrap.min.css'?>">
  <link rel="stylesheet" href="<?=FRONTEND.'css/try.css'?>">
  <script src="<?=FRONTEND.'js/respond.js'?>"></script>
  <script src="<?=FRONTEND.'js/bootstrap.min.js'?>"></script>
  <script src="<?=FRONTEND.'js/jquery-1.11.0.min.js'?>"></script>
  <script src="<?=BACKEND.'ckeditor/ckeditor.js'?>"></script>
  <style type="text/css">
    .maincontent{
      padding: 8px 12px;
      border-radius:0px;
      /*border: 1px solid #cccccc;*/
    }
    .nav-tabs > li > a, .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{
      border-radius: 0px;
      border-bottom: 1px solid #cccccc;
    }
    label{
      font-weight: normal;
    }
    .error {
      color: red;
      font-style: italic;
      font-size: 12px;
    }
  </style>
</head>
<body>
  <?php $sessUserId = $this->session->userdata('userid');  ?>
  <!--Top Fixed Nav Bar-->
  <div id="top-nav" class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?=site_url('dashboard'); ?>" >
          <span class='glyphicon glyphicon-home collapsed-element'></span>&nbsp;&nbsp;&nbsp;
          <?php echo 'Welcome, '.$userdetails[0]->first_name.' '.$userdetails[0]->last_name; ?>
        </a>
      </div><!--navbar-header ends-->
      <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-right">
          <li>
            <a href="<?=site_url('home'); ?>"> <!--<i class="glyphicon glyphicon-pencil"></i>-->
              www.electrobid.com
            </a>
          </li>
          <li>
            <!-- <a href="<?=site_url('users/logout'); ?>"><i class="glyphicon glyphicon-lock"></i>
              Logout
            </a> -->
            <button type="button" class="btn btn-primary pull-right dropdown-toggle headerbutton" data-toggle="dropdown" style=" margin-top:-1px;padding-top: 1.1em;padding-bottom:1em; border-radius:0px;"><?php echo $userdetails[0]->email; ?>&nbsp;<span class="caret"></span></button>
            <ul class="dropdown-menu pull-right">
              <li><a href="<?=site_url('profile'); ?>">&nbsp;&nbsp;View Your Profile</a></li>
              <li><a href="<?=site_url('users/logout'); ?>">&nbsp;&nbsp;Logout</a></li>
            </ul>
          </li>
        </ul><!--right nav ends-->
      </div><!--navbar-collapse ends-->
    </div><!-- /container -->
  </div><!--top nav ends-->

  