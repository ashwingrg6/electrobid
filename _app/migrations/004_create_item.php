<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_Item extends CI_Migration {

	public function up()
	{
		$this->dbforge->drop_table('item');
		$this->dbforge->add_field(array(
			'item_id' => array(
				'type'           => 'MEDIUMINT',
				'constraint'     => '8',
				'unsigned'       => TRUE,
				'auto_increment' => TRUE
			),
			'category_id' => array(
				'type'           => 'MEDIUMINT',
				'constraint'     => '8',
				'unsigned'       => TRUE,
			),
			'product_id' => array(
				'type'           => 'MEDIUMINT',
				'constraint'     => '8',
				'unsigned'       => TRUE,
			),
			'user_id' => array(
				'type'           => 'MEDIUMINT',
				'constraint'     => '8',
				'unsigned'       => TRUE,
			),
			'item_name' => array(
				'type'       => 'VARCHAR',
				'constraint' => '50'
			),
			'item_price' => array(
				'type'       => 'DECIMAL',
				'constraint' => '12'
			),
			'item_image' => array(
				'type'       => 'VARCHAR',
				'constraint' => '255'
			),
			'item_description' => array(
				'type'       => 'TEXT'
			),
			'item_status' => array(
				'type' => "ENUM('sold','unsold')",
        'default' => 'unsold'
			),
			'item_timestamp' => array(
				'type' => 'VARCHAR',
				'constraint' => '25'
			)
		));
		$this->dbforge->add_key('item_id', TRUE);
		$this->dbforge->create_table('item');
		$this->db->query('ALTER TABLE `item` ADD INDEX `item` (`category_id`,`product_id`,`user_id`);');
		//add foreign key
		$this->db->query('ALTER TABLE item ADD FOREIGN KEY (category_id) REFERENCES category(category_id) ON DELETE CASCADE ON UPDATE CASCADE;');
		//$this->db->query('ALTER TABLE `item` ADD INDEX `item` (`product_id`);');
		//add foreign key
		$this->db->query('ALTER TABLE item ADD FOREIGN KEY (product_id) REFERENCES product(product_id) ON DELETE CASCADE ON UPDATE CASCADE;');
		//$this->db->query('ALTER TABLE `item` ADD INDEX `item` (`user_id`);');
		$this->db->query('ALTER TABLE item ADD FOREIGN KEY (user_id) REFERENCES user(user_id) ON DELETE CASCADE ON UPDATE CASCADE;');

	}

	public function down()
	{
		$this->dbforge->drop_table('item');
	}
}
