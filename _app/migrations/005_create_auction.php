<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_Auction extends CI_Migration {

	public function up()
	{
		$this->dbforge->drop_table('auction');
		$this->dbforge->add_field(array(
			'auction_id' => array(
				'type'           => 'MEDIUMINT',
				'constraint'     => '8',
				'unsigned'       => TRUE,
				'auto_increment' => TRUE
			),
			'item_id' => array(
				'type'           => 'MEDIUMINT',
				'constraint'     => '8',
				'unsigned'       => TRUE,
			),
			'user_id' => array(
				'type'           => 'MEDIUMINT',
				'constraint'     => '8',
				'unsigned'       => TRUE,
			),
			'create_date' => array(
				'type' =>	'VARCHAR',
				'constraint' => '30'
			),
			'close_date' => array(
				'type' =>	'VARCHAR',
				'constraint' => '30'
			),
			'start_price' => array(
				'type' => 'DECIMAL',
				'constraint' => '12'
			),
			'auction_status' => array(
				'type' => "ENUM('open','closed')",
				'default' => 'open'
			),
			'auction_timestamp' => array(
				'type' => 'VARCHAR',
				'constraint' => '25'
			)
		));
		$this->dbforge->add_key('auction_id', TRUE);
		$this->dbforge->create_table('auction');
		$this->db->query('ALTER TABLE `auction` ADD INDEX `auction` (`item_id`,`user_id`);');
		//add foreign key
		$this->db->query('ALTER TABLE auction ADD FOREIGN KEY (item_id) REFERENCES item(item_id) ON DELETE CASCADE ON UPDATE CASCADE;');
		$this->db->query('ALTER TABLE auction ADD FOREIGN KEY (user_id) REFERENCES user(user_id) ON DELETE CASCADE ON UPDATE CASCADE;');

	}

	public function down()
	{
		$this->dbforge->drop_table('auction');
	}
}
