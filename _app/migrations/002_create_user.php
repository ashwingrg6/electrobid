<?php
class Migration_Create_user extends CI_Migration
{

    public function up (){
      $this->dbforge->drop_table('user');
      $data = array(
            'user_id' => array(
              'type' => 'MEDIUMINT',
              'constraint' => 10,
              'unsigned' => TRUE,
              'auto_increment' => TRUE,
              'null'=>FALSE
            ),
            'first_name' => array(
              'type' => 'VARCHAR',
              'constraint' => '30'
            ),
            'last_name' => array(
              'type' => 'VARCHAR',
              'constraint' => '30'
            ),
            'street' => array(
              'type'       => 'VARCHAR',
              'constraint' => '100',
            ),
            'address' => array(
              'type' => 'VARCHAR',
              'constraint' => '255'
            ),
            'postcode' => array(
              'type' => 'MEDIUMINT',
              'constraint' => '8'
            ),
            'mobile_no' => array(
              'type' => 'VARCHAR',
              'constraint' => '10'
            ),
            'other_no' => array(
              'type' => 'VARCHAR',
              'constraint' => '9'
            ),
            'email' => array(
              'type' => 'VARCHAR',
              'constraint' => '255'
            ),
            'login_username' => array(
              'type' => 'VARCHAR',
              'constraint' => '30'
            ),
            'user_type' => array(
              'type' => "ENUM('admin','user')",
              'default' => 'admin'
            ),
            'login_password' => array(
              'type' => 'VARCHAR',
              'constraint' => '512'
            ),
            'date_of_join' => array(
              'type' => 'VARCHAR',
              'constraint' => '30'
            )
      );
      $this->dbforge->add_field($data);
      $this->dbforge->add_key('user_id', TRUE);
      $this->dbforge->create_table('user');

      $insertData = array(
        'user_id' => '1',
        'first_name' => 'Super',
        'last_name' => 'Admin',
        'street' => 'Lazimpat',
        'address' => 'lazimpat-2, Kathmandu, Nepal',
        'postcode' => '977',
        'mobile_no' => '9888777666',
        'other_no' => '014441561',
        'email' => 'admin@admin.com',
        'login_username' => 'superAdmin',
        'user_type' => 'admin',
        'login_password' => 'b109f3bbbc244eb82441917ed06d618b9008dd09b3befd1b5e07394c706a8bb980b1d7785e5976ec049b46df5f1326af5a2ea6d103fd07c95385ffab0cacbc86',
        'date_of_join' => '2014-12-21',
      );
      $this->db->insert('user', $insertData);
    }

    public function down ()
    {
        $this->dbforge->drop_table('user');
    }
}