<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_Purchase extends CI_Migration {

	public function up()
	{
		$this->dbforge->drop_table('purchase');
		$this->dbforge->add_field(array(
			'purchase_id' => array(
				'type'           => 'MEDIUMINT',
				'constraint'     => '8',
				'unsigned'       => TRUE,
				'auto_increment' => TRUE
			),
			'item_id' => array(
				'type'           => 'MEDIUMINT',
				'constraint'     => '8',
				'unsigned'       => TRUE,
			),
			'user_id' => array(
				'type'           => 'MEDIUMINT',
				'constraint'     => '8',
				'unsigned'       => TRUE,
			),
			'purchase_date' => array(
				'type'       => 'VARCHAR',
				'constraint' => '25'
			),
			'purchase_timestamp' => array(
				'type' => 'VARCHAR',
				'constraint' => '15'
			),
			'payment_status' => array(
				'type'       => "ENUM('pending','paid')",
				'default' => 'pending'
			)
		));
		$this->dbforge->add_key('purchase_id', TRUE);
		$this->dbforge->create_table('purchase');
		$this->db->query('ALTER TABLE `purchase` ADD INDEX `purchase` (`item_id`,`user_id`);');
		//add foreign key
		$this->db->query('ALTER TABLE purchase ADD FOREIGN KEY (item_id) REFERENCES item(item_id) ON DELETE CASCADE ON UPDATE CASCADE;');
		//$this->db->query('ALTER TABLE `item` ADD INDEX `item` (`product_id`);');
		//add foreign key
		$this->db->query('ALTER TABLE purchase ADD FOREIGN KEY (user_id) REFERENCES user(user_id) ON DELETE CASCADE ON UPDATE CASCADE;');
	}

	public function down()
	{
		$this->dbforge->drop_table('purchase');
	}
}
