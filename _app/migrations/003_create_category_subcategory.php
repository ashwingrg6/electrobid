<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_Category_SubCategory extends CI_Migration {

	public function up()
	{
		$this->dbforge->drop_table('category');
		$this->dbforge->add_field(array(
			'category_id' => array(
				'type'           => 'MEDIUMINT',
				'constraint'     => '8',
				'unsigned'       => TRUE,
				'auto_increment' => TRUE
			),
			'category_name' => array(
				'type'       => 'VARCHAR',
				'constraint' => '50'
			),
			'category_description' => array(
				'type'       => 'TEXT'
			)
		));
		$this->dbforge->add_key('category_id', TRUE);
		$this->dbforge->create_table('category');

		$this->dbforge->drop_table('product');
		$this->dbforge->add_field(array(
			'product_id' => array(
				'type'           => 'MEDIUMINT',
				'constraint'     => '8',
				'unsigned'       => TRUE,
				'auto_increment' => TRUE
			),
			'category_id' => array(
				'type'           => 'MEDIUMINT',
				'constraint'     => '8',
				'unsigned'       => TRUE
			),
			'product_name' => array(
				'type'       => 'VARCHAR',
				'constraint' => '50'
			),
			'product_description' => array(
				'type'       => 'TEXT'
			)
		));
		$this->dbforge->add_key('product_id', TRUE);
		$this->dbforge->create_table('product');

		$this->db->query('ALTER TABLE `product` ADD INDEX `product` (`category_id`);');
		//add foreign key
		$this->db->query('ALTER TABLE product ADD FOREIGN KEY (category_id) REFERENCES category(category_id) ON DELETE CASCADE ON UPDATE CASCADE;');
	}

	public function down()
	{
		$this->dbforge->drop_table('category');
		$this->dbforge->drop_table('product');
	}
}
