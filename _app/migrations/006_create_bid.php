<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_Bid extends CI_Migration {

	public function up()
	{
		$this->dbforge->drop_table('bid');
		$this->dbforge->add_field(array(
			'bid_id' => array(
				'type'           => 'MEDIUMINT',
				'constraint'     => '8',
				'unsigned'       => TRUE,
				'auto_increment' => TRUE
			),
			'item_id' => array(
				'type'           => 'MEDIUMINT',
				'constraint'     => '8',
				'unsigned'       => TRUE,
			),
			'user_id' => array(
				'type'           => 'MEDIUMINT',
				'constraint'     => '8',
				'unsigned'       => TRUE,
			),
			'bid_price' => array(
				'type'       => 'DECIMAL',
				'constraint' => '12'
			),
			'bid_date' => array(
				'type'       => 'VARCHAR',
				'constraint' => '25'
			),
			'bid_timestamp' => array(
				'type'       => 'VARCHAR',
				'constraint' => '11'
			)
		));
		$this->dbforge->add_key('bid_id', TRUE);
		$this->dbforge->create_table('bid');
		$this->db->query('ALTER TABLE `bid` ADD INDEX `bid` (`item_id`,`user_id`);');
		//add foreign key
		$this->db->query('ALTER TABLE bid ADD FOREIGN KEY (item_id) REFERENCES item(item_id) ON DELETE CASCADE ON UPDATE CASCADE;');
		//$this->db->query('ALTER TABLE `item` ADD INDEX `item` (`product_id`);');
		//add foreign key
		$this->db->query('ALTER TABLE bid ADD FOREIGN KEY (user_id) REFERENCES user(user_id) ON DELETE CASCADE ON UPDATE CASCADE;');
	}

	public function down()
	{
		$this->dbforge->drop_table('bid');
	}
}
