<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product_model extends CI_Model {

	//sets validation rules for new Product
  public $validation_newProduct = array(
            array(
              'field' => 'product_name',
              'label' => 'product name',
              'rules' => 'required|trim|sanitize|is_unique[product.product_name]'
            ),
            array(
              'field' => 'category',
              'label' => 'category name',
              'rules' => 'required|trim|sanitize'
            ),
            array(
              'field' => 'pdescription',
              'label' => 'description',
              'rules' => 'trim|sanitize'
            )
  );

  /**
   *
   * @return 
   */
  public function insertProduct($insertData){
  	$query = $this->db->insert('product', $insertData);
  	if($query){
  		return true;
  	}
  }

  /**
   *
   * @return 
   */
  public function productTable(){
  	$query = $this->db->select('*')
											->from('product')
											->join('category', 'category.category_id = product.category_id')
											->get();
		return $query->result();
  }

  public function productByCategory($categoryId){
  	$query = $this->db->select('*')
											->where('product.category_id', $categoryId)
											->from('product')
											->join('category', 'category.category_id = product.category_id')
											->get();
		return $query->result();
  }

  /**
   *
   * @return 
   */
  public function deleteProduct($productId){
  	$query = $this->db->delete('product', array('product_id' => $productId));
		if($query){
			return true;
		}
  }


}

/* End of file product_model.php */
/* Location: .//C/xampp/htdocs/electrobid/_app/backend/products/models/product_model.php */