<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product extends MX_Controller {

	/**************** constructor function ************/
	public function __construct(){
		parent::__construct();
		Modules::run('category/construct');
		$this->load->model('product_model','product');
		$this->load->model('category_model','category');
	}
	/************************ constructor ends ****************************/

	/**
	 *
	 * @return 
	 */
	public function index(){
		if($_POST){
			$product_name = $this->input->post('product_name', TRUE);
			$category = $this->input->post('category', TRUE);
			$pdescription = $this->input->post('pdescription', TRUE);
			$this->form_validation->set_rules($this->product->validation_newProduct);
			if($this->form_validation->run() == TRUE):
				$insertData = array(
					'category_id' => $category,
					'product_name' => $product_name,
					'product_description' => $pdescription
				);
				$insertFlag = $this->product->insertProduct($insertData);
				if($insertFlag){
					$this->session->set_flashdata('productAdded', 'Success: New product has been added successfully.');
					redirect('product');
				};
			else:
				$this->form_validation->set_error_delimiters('','');
				$this->commonFunction();
			endif;
		}
		else{
			$this->commonFunction();
		}
	}

	/**
	 *
	 * @return 
	 */
	public function delete($productId){
		$deleteFlag = $this->product->deleteProduct($productId);
		if($deleteFlag){
			$this->session->set_flashdata('productDeleted', 'Success: Selected product has been deleted successfully.');
			redirect('product');
		}
	}

	/**
	 *
	 * @return 
	 */
	public function filter(){
		if($_POST):
			$categoryId = $this->input->post('category', TRUE);
			if($categoryId == 'all'):
				redirect('product');
			else:
				// $d = $this->product->productByCategory($categoryId);
				$data['allproducts'] = $this->product->productByCategory($categoryId);
				$data['allcategories'] = $this->category->allCategories();
				$data['userdetails'] = modules::run('users/dataforHeader');
				$this->template->load('backend/template/main', 'product_index_view', $data);
			endif;
		else:
			redirect('product');
		endif;
	}

	/**
	 *
	 * @return 
	 */
	public function commonFunction(){
		$data['allproducts'] = $this->product->productTable();
		$data['allcategories'] = $this->category->allCategories();
		$data['userdetails'] = modules::run('users/dataforHeader');
		$this->template->load('backend/template/main', 'product_index_view', $data);
	}

	/**
	 *
	 * @return 
	 */
	public function returnAllProducts(){
		return $this->product->productTable();
	}

	/**
	 *
	 * @return 
	 */
	public function returnProductByCategory($categoryId){
		return $this->product->productByCategory($categoryId);
	}
}

/* End of file product.php */
/* Location: .//C/xampp/htdocs/electrobid/_app/backend/products/controllers/product.php */