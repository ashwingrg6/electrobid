      <!--<p class="well lead">asdf</p>
      <p class="alert alert-dismissable alert-danger">asfadsfas</p>
      <p class="alert alert-dismissable alert-danger">asdfadfda</p>
      -->
      <h2>Products</h2>
      <hr>
      <?php if($this->session->flashdata('productAdded')){ ?>
      <!--notify msg-->
      <div class="alert alert-success alert-dismissable" style="padding:8px; border-radius:0px;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo $this->session->flashdata('productAdded'); ?>
      </div><!--msg notify ends-->
      <?php } ?>
      <!--form to add new category-->
      <div class="md-col-8">
        <form method="post" name="filterUser" action="<?php echo current_url(); ?>">
          <div class="form-group" >
            <label for="Product Name" class="control-label col-md-1" style="text-align:left;">Product Name :</label>
            <div class="col-md-2" style="padding:0px;">
              <input type="text" class="form-control" name="product_name" id="product_name" value="<?php echo set_value('product_name'); ?>" placeholder="Enter Category Name" style="border-radius:0px;">
              <span class="error" >
                <?php echo form_error('product_name'); ?>
              </span>
            </div>
            <label for="Product Name" class="control-label col-md-1" style="text-align:left;">Category Name :</label>
            <div class="col-md-3">
              <select name="category" id="category" class="form-control" style="border-radius:0px;">
                <option value="">Please Select</option>
                <?php foreach ($allcategories as $key => $value) { ?>
                <option value="<?php echo $value->category_id; ?>" <?php echo set_select('category', $value->category_id); ?>><?php echo $value->category_name ?></option>
                <?php } ?>
              </select>
              <span class="error">
                <?php echo form_error('category'); ?>
              </span>
            </div>
          </div><br><br><br>
          <div class="form-group" >
            <label for="Product Description" class="control-label col-md-1" style="text-align:left;">Product Description:</label>
            <div class="col-md-4" style="padding:0px;">
              <textarea name="pdescription" cols="" rows="2" class="form-control" style="border-radius:0px;"><?php echo set_value('pdescription'); ?></textarea>
            </div>
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary" style="border-radius:0px; margin-left:4px;">ADD NEW</button>
            <button type="reset" class="btn btn-danger" style="border-radius:0px;" onclick="window.location.href='<?php echo site_url('product'); ?>'">CANCEL</button>
          </div>
        </form>
      </div><!--new category ends here-->

      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 maincontent" style="padding:10px 0; border-top:1px solid #dddddd; margin-top:10px;">
        <!--filter by category-->
        <div class="row">
          <div class="col-md-12">
            <form method="post" name="filterUser" action="<?=site_url('product/filter'); ?>">
              <p>
                <span class="col-lg-1" style="padding:0px;">
                  <label for=""><strong>Filter By:</strong></label>
                </span>
                <span class="col-lg-2">
                  <select name="category" id="category" class="form-control" style="border-radius:0px;">
                    <option value="all">All</option>
                    <?php foreach ($allcategories as $key => $value) { ?>
                      <option value="<?php echo $value->category_id; ?>"><?php echo $value->category_name; ?></option>
                    <?php } ?>
                  </select>
                </span>
                <span class="col-lg-1">
                  <button type="submit" class="btn btn-primary btn-primary" value="" style="border-radius:0px;">Filter</button>
                </span>
              </p>
            </form>
          </div>
        </div><!--filter by category ends here-->
        <?php if($this->session->flashdata('productDeleted')){ ?>
        <!--notify <msg-->
        <div class="alert alert-success alert-dismissable" style="padding:8px; border-radius:0px; margin-top:5px;">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <?php echo $this->session->flashdata('productDeleted'); ?>
        </div><!--msg notify ends-->
        <?php } ?>
        <!--all products-->
        <div class="panel panel-default" style="margin-top:5px;">
          <div class="panel-heading" ><strong>All Products</strong></div>
          <!--panel-heading ends-->
          <div class="panel-body">
            <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover" id="dataTables-example2">
                <thead>
                  <tr>
                    <th>S-N</th>
                    <th>Category</th>
                    <th>Product Name</th>
                    <th>Product Description</th>
                    <th width="130">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $sn = 1;
                  foreach ($allproducts as $key => $value): ?>
                  <tr>
                    <td style="text-align:center;"><?php echo $sn; ?></td>
                    <td><?php echo $value->category_name; ?></td>
                    <td><?php echo $value->product_name; ?></td>
                    <td><?php echo $value->product_description; ?></td>
                    <td class="center" style="margin:0px; padding:5px 0px 0px 7px;">
                      <!--<button class="btn btn-primary btn-sm" onclick="window.location.href='<?php //echo site_url('product/edit').'/'.$value->product_id; ?>';">Edit</button>-->
                      <button class="btn btn-danger btn-sm" onclick="window.location.href='<?php echo site_url('product/delete').'/'.$value->product_id; ?>';">Delete</button>
                    </td>
                  </tr>
                  <?php $sn++;endforeach; ?>
                </tbody>
              </table>
            </div>
            <!-- table-responsive ends -->
          </div>
          <!-- /.panel-body -->
        </div><!--all products-->
      </div><!--maincon-->
      <hr>
