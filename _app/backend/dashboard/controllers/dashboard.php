<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MX_Controller {

	/**************** constructor function ************/
	public function __construct(){
		parent::__construct();
		Modules::run('users/checkLoginSession');
		$this->load->model('dashboard_model','dashboard');
	}
	/************************ constructor ends ****************************/
	
	/**
	 *
	 * @return 
	 */
	public function index() {
		$data['availableauctions'] = $this->dashboard->getAvailableAuctions();
		$data['auctionshistory'] = $this->dashboard->getAuctionsById();
		// print_r($data['auctionshistory']);die();
		// $this->output->enable_profiler(TRUE);
		$data['userdetails'] = modules::run('users/dataforHeader');
		$this->template->load('backend/template/main', 'dashboard_view', $data);
	}

	/**
	 *
	 * @return 
	 */
	public function bids($item_id){
		$data['bids'] = $this->dashboard->getBidsByItem($item_id);
		$data['auctionDetails'] = $this->dashboard->getItemById($item_id);
		$data['userdetails'] = modules::run('users/dataforHeader');
		$this->template->load('backend/template/main', 'view_bids_view', $data);
	}

	/**
	 *
	 * @return 
	 */
	public function offers($item_id){
		$data['offers'] = $this->dashboard->getOffersByItem($item_id);
		$data['auctionDetails'] = $this->dashboard->getItemById($item_id);
		$data['userdetails'] = modules::run('users/dataforHeader');
		$this->template->load('backend/template/main', 'view_offers_view', $data);
	}

}

/* End of file admin.php */
/* Location: .//C/xampp/htdocs/electro/_app/backend/dashboard/controllers/admin.php */