<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_model extends CI_Model{


	 /**
   *
   * @return 
   */
  public function getAvailableAuctions(){
    $sessUserId = $this->session->userdata('userid');
    $whereArr = array(
      'item.item_status' => 'unsold',
      'item.user_id !=' => $sessUserId,
      'auction.auction_status' => 'open'
    );
    $query = $this->db->select('*')
                      ->where($whereArr)
                      ->from('item')
                      ->join('category', "item.category_id = category.category_id")
                      ->join('product', "item.product_id = product.product_id")
                      ->join('auction', "auction.item_id = item.item_id")
                      ->order_by('item_timestamp', 'DESC')
                      ->get();
    return $query->result();
  }

  /**
   *
   * @return 
   */
  public function getAuctionsById(){
    $sessUserId = $this->session->userdata('userid');
    $whereArr = array(
      'item.user_id ' => $sessUserId,
    );
    $query = $this->db->select('*')
                      ->where($whereArr)
                      ->from('item')
                      ->join('category', "item.category_id = category.category_id")
                      ->join('product', "item.product_id = product.product_id")
                      ->join('auction', "auction.item_id = item.item_id")
                      ->order_by('item_timestamp', 'DESC')
                      ->get();
    return $query->result();
  }

  /**
   *
   * @return 
   */
  public function getBidsByItem($item_id){
    $query = $this->db->select('*')
                      ->where('bid.item_id', $item_id)
                      ->from('bid')
                      ->join('user','bid.user_id = user.user_id','left')
                      ->order_by('bid_timestamp','desc')
                      ->get();
    return $query->result();
  }

  /**
   *
   * @return 
   */
  public function getItemById($item_id){
    $query = $this->db->select('*')
                      ->where('item.item_id', $item_id)
                      ->from('item')
                      ->join('auction','item.item_id = auction.auction_id','left')
                      ->get();
    return $query->result();
  }

  /**
   *
   * @return 
   */
  public function getOffersByItem($item_id){
    $query = $this->db->select('*')
                      ->where('purchase.item_id', $item_id)
                      ->from('purchase')
                      ->join('user','purchase.user_id = user.user_id','left')
                      ->order_by('purchase_timestamp','desc')
                      ->get();
    return $query->result();
  }

}

/* End of file dashboard_model.php */
/* Location: .//C/xampp/htdocs/electrobid/_app/backend/dashboard/models/dashboard_model.php */