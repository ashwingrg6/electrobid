      <h2>View Offers - <?php echo $auctionDetails[0]->item_name; ?></h2><hr>
      <p>Item Price: <strong><?php echo $auctionDetails[0]->item_price; ?></strong></p>
      <p>Item Uploaded Date: <strong><?php echo $auctionDetails[0]->create_date; ?></strong></p>
      <p>Bid Start Price: <strong><?php echo $auctionDetails[0]->start_price; ?></strong></p>
      <p>Bid Close Date: <strong><?php echo $auctionDetails[0]->close_date; ?></strong></p>
      <p>Item Description:<?php echo $auctionDetails[0]->item_description; ?></p>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 maincontent" style="padding-left: 0px;">
        <!--all bids-->
        <div class="panel panel-default">
          <div class="panel-heading"><strong>Bids</strong></div>
          <!--panel-heading ends-->
          <div class="panel-body">
            <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                <thead>
                  <tr>
                    <th width="10">S-N</th>
                    <th width="100">Offered By</th>
                    <th width="100">Offered Date</th>
                    <th width="130">Payment Status</th>
                    <th width="130">Edit Payment Status</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                $sn = 1;
                foreach ($offers as $key => $value) { ?>
                  <tr class="odd">
                    <td style="text-align:center;"><?php echo $sn; ?></td>
                    <td><?php echo $value->first_name.' '.$value->last_name; ?></td>
                    <td><?php echo $value->purchase_date; ?></td>
                    <td><?php echo $value->payment_status; ?></td>
                    <td>
                      <?php if($value->payment_status == 'pending'){ ?>
                      <button class="btn btn-primary btn-sm" onclick="window.location.href='<?php echo site_url('dashboard/bids').'/'.$value->item_id; ?>';">Paid</button><?php } elseif($value->payment_status == 'paid'){ ?>
                      <button class="btn btn-primary btn-sm" onclick="window.location.href='<?php echo site_url('dashboard/offers').'/'.$value->item_id;  ?>';" style="margin-top:3px;">Pending</button>
                    <?php }else { echo ""; } ?>
                    </td>
                  </tr>
                <?php $sn++;} ?>
                </tbody>
              </table>
            </div>
            <!-- table-responsive ends -->
          </div>
          <!-- /.panel-body -->
        </div><!--all bids-->