      <!--<p class="well lead">asdf</p>
      <p class="alert alert-dismissable alert-danger">asfadsfas</p>
      <p class="alert alert-dismissable alert-danger">asdfadfda</p>
      -->
      <h2>Dashboard</h2><hr>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde necessitatibus temporibus consequuntur incidunt harum quaerat minima nulla obcaecati facilis quidem vitae nesciunt cumque quia laudantium, non impedit, quas.
      </p>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 maincontent" style="padding-left: 0px;">
      <?php if($userdetails[0]->user_type != 'admin'): ?>
        <!--auctions history-->
        <div class="panel panel-default">
          <div class="panel-heading"><strong>Your auctions history till now.</strong></div>
          <!--panel-heading ends-->
          <div class="panel-body">
            <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                <thead>
                  <tr>
                    <th width="60">S-N</th>
                    <th width="100">Category</th>
                    <th width="100">Product</th>
                    <th width="130">Item Name</th>
                    <th width="70">Image</th>
                    <th width="100">Bid Start Price</th>
                    <th width="115">Bid Close Date</th>
                    <th width="115">Action</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                $sn = 1;
                foreach ($auctionshistory as $key => $value) { ?>
                  <tr class="odd">
                    <td style="text-align:center;"><?php echo $sn; ?></td>
                    <td><?php echo $value->category_name;  ?></td>
                    <td><?php echo $value->product_name; ?></td>
                    <td><?php echo $value->item_name; ?></td>
                    <td><img src="<?php echo URL1.'uploads/'.$value->item_image; ?>" alt=""  height="50" width="80"></td>
                    <td><?php echo $value->start_price; ?></td>
                    <td><?php echo $value->close_date; ?></td>
                    <td>
                      <button class="btn btn-default btn-xs" onclick="window.location.href='<?php echo site_url('dashboard/bids').'/'.$value->item_id; ?>';">View All Bids</button>
                      <button class="btn btn-default btn-xs" onclick="window.location.href='<?php echo site_url('dashboard/offers').'/'.$value->item_id;  ?>';">View All Offers</button>
                    </td>
                  </tr>
                <?php $sn++;} ?>
                </tbody>
              </table>
            </div>
            <!-- table-responsive ends -->
          </div>
          <!-- /.panel-body -->
        </div><!--auctions history ends here-->
        <hr>
      <?php endif; ?>
        <!--currently available auctions-->
        <div class="panel panel-default">
          <div class="panel-heading"><strong>Currently available auctions...</strong></div>
          <!--panel-heading ends-->
          <div class="panel-body">
            <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover" id="dataTables-example2">
               <thead>
                  <tr>
                    <th width="60">S-N</th>
                    <th width="130">Product</th>
                    <th width="200">Item Name</th>
                    <th width="100">Item Image</th>
                    <th width="200">Created Date</th>
                    <th width="200">Close Date</th>
                    <th width="200">Item Price</th>
                    <th width="100">Bid Start Price</th>
                    <th width="115">Bid Close Date</th>
                  </tr>
                </thead>
                <tbody>
                <?php 
                $sn = 1;
                foreach ($availableauctions as $key => $value) { ?>
                  <tr class="odd">
                    <td style="text-align:center;"><?php echo $sn; ?></td>
                    <td><?php echo $value->product_name; ?></td>
                    <td><?php echo $value->item_name; ?></td>
                    <td><img src="<?php echo URL1.'uploads/'.$value->item_image; ?>" alt="" height="50" width="80"></td>
                    <td><?php echo $value->create_date; ?></td>
                    <td><?php echo $value->close_date; ?></td>
                    <td><?php echo $value->item_price; ?></td>
                    <td><?php echo $value->start_price; ?></td>
                    <td><?php echo $value->close_date; ?></td>
                  </tr>
                <?php $sn++; } ?>
                </tbody>
              </table>
            </div>
            <!-- table-responsive ends -->
          </div>
          <!-- /.panel-body -->
        </div><!--currently available auctions-->
      </div><!--maincon-->
      <hr>
