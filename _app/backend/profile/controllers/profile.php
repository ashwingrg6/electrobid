<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends MX_Controller {

	/**************** constructor function ************/
	public function __construct(){
		parent::__construct();
		Modules::run('users/checkLoginSession');
		$this->load->model('profile_model','profile');
		$this->load->model('user_model','user');
	}
	/************************ constructor ends ****************************/

	/**
	 *
	 * @return 
	 */
	public function index() {
		// $this->output->enable_profiler(TRUE);
		$data['userdetails'] = modules::run('users/dataforHeader');
		$this->template->load('backend/template/main', 'profile_index_view', $data);
	}

	/**
	 *
	 * @return 
	 */
	public function loggedinUserUpdate(){
		if($_POST){
      // $this->form_validation->set_rules($this->user->validation_newUser);
			$loggedinUserId = $this->input->post('loggedinUserId', TRUE);
			$email = $this->input->post('nemail', TRUE);
			$username = $this->input->post('nusername', TRUE);
			$userInfo = $this->user->getUserById($loggedinUserId);
			$firstname = $this->input->post('nfirstname', TRUE);
			$lastname = $this->input->post('nlastname', TRUE);
			$street = $this->input->post('nstreet', TRUE);
			$address = $this->input->post('nfulladdress', TRUE);
			$postcode = $this->input->post('npostcode', TRUE);
			$mobileno = $this->input->post('nmobileno', TRUE);
			$otherno = $this->input->post('nothertel', TRUE);
			$updateData = array(
				'first_name' => $firstname,
				'last_name' => $lastname,
				'street' => $street,
				'address' => $address,
				'postcode' => $postcode,
				'mobile_no' => $mobileno,
				'other_no' => $otherno,
				'email' => $email,
				'login_username' => $username
			);
    	if($userInfo[0]->email == $email && $userInfo[0]->login_username == $username){
    		$this->form_validation->set_rules($this->profile->validation_loggedinUserUpdate3);
    		if($this->form_validation->run() == TRUE):
    			$updateFlag = $this->user->updateUser($loggedinUserId, $updateData);
    			if($updateFlag)
    				$this->session->set_flashdata('loggedinUserUpdated', 'Success: details has been updated successfully');
    				redirect('profile');
    		else:
        	$this->form_validation->set_error_delimiters('','');
					$this->session->set_flashdata('loggedinUserUpdateError1', 'Error: Validation Error! All fields are required. Please try again.');
					redirect('profile');
    		endif;
    	}
    	else{
    		if($userInfo[0]->email == $email){
					$this->form_validation->set_rules($this->profile->validation_loggedinUserUpdate1);
	    		if($this->form_validation->run() == TRUE):
	    			$updateFlag = $this->user->updateUser($loggedinUserId, $updateData);
	    			if($updateFlag)
	    				$this->session->set_flashdata('loggedinUserUpdated', 'Success: details has been updated successfully');
	    				redirect('profile');
	    		else:
	    			echo "error";die();
	        	$this->form_validation->set_error_delimiters('','');
						$this->session->set_flashdata('loggedinUserUpdateError1', 'Error: Validation Error! username field must be unique and all fields are required. Please try again.');
						redirect('profile');
	    		endif;
	    	}
	    	elseif($userInfo[0]->login_username == $username){
	    		$this->form_validation->set_rules($this->profile->validation_loggedinUserUpdate2);
	    		if($this->form_validation->run() == TRUE):
	    			$updateFlag = $this->user->updateUser($loggedinUserId, $updateData);
	    			if($updateFlag)
	    				$this->session->set_flashdata('loggedinUserUpdated', 'Success: details has been updated successfully');
	    				redirect('profile');
	    		else:
	        	$this->form_validation->set_error_delimiters('','');
						$this->session->set_flashdata('loggedinUserUpdateError1', 'Error: Validation Error! email field must be unique and all fields are required. Please try again.');
						redirect('profile');
	    		endif;
	    	}
	    	elseif($userInfo[0]->email != $email && $userInfo[0]->login_username != $username){
	    		$this->form_validation->set_rules($this->profile->validation_loggedinUserUpdate4);
	    		if($this->form_validation->run() == TRUE):
	    			$updateFlag = $this->user->updateUser($loggedinUserId, $updateData);
	    			if($updateFlag)
	    				$this->session->set_flashdata('loggedinUserUpdated', 'Success: details has been updated successfully');
	    				redirect('profile');
	    		else:
	        	$this->form_validation->set_error_delimiters('','');
						$this->session->set_flashdata('loggedinUserUpdateError1', 'Error: Validation Error! email and username field must be unique and all fields are required. Please try again.');
						redirect('profile');
	    		endif;
	    	}
    	}

		} //if ends, post
	}

}

/* End of file profile.php */
/* Location: .//C/xampp/htdocs/electrobid/_app/backend/profile/controllers/profile.php */