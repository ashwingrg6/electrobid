      <!--<p class="well lead">asdf</p>
      <p class="alert alert-dismissable alert-danger">asfadsfas</p>
      <p class="alert alert-dismissable alert-danger">asdfadfda</p>
      -->
      <h2>Your Profile - <?php echo $userdetails[0]->first_name.' '.$userdetails[0]->last_name; ?></h2><hr>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde necessitatibus temporibus consequuntur incidunt harum quaerat minima nulla obcaecati facilis quidem vitae nesciunt cumque quia laudantium, non impedit, quas.
      </p><?php echo validation_errors(); ?>
      <?php if($this->session->flashdata('loggedinUserUpdateError1')){ ?>
      <!--notify msg-->
      <div class="alert alert-danger alert-dismissable" style="padding:8px; border-radius:0px;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo $this->session->flashdata('loggedinUserUpdateError1'); ?>
      </div><!--msg notify ends-->
      <?php } ?>
      <?php if($this->session->flashdata('loggedinUserUpdated')){ ?>
      <!--notify msg-->
      <div class="alert alert-success alert-dismissable" style="padding:8px; border-radius:0px;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo $this->session->flashdata('loggedinUserUpdated'); ?>
      </div><!--msg notify ends-->
      <?php } ?>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 maincontent" style="padding-left: 0px;">
        <ul class="nav nav-tabs">
          <li class="<?php if($this->session->flashdata('loggedinUserUpdateError1')){
            echo " ";
            }else{
              echo "active";
            }
            ?>">
            <a href="#profile" data-toggle="tab">Profile Details</a>
          </li>
          <li class="<?php if($this->session->flashdata('loggedinUserUpdateError1')){
            echo "active";
            } ?>">
            <a href="#editprofile" data-toggle="tab">Edit Profile</a>
          </li>
          <li class="">
            <a href="#editpsw" data-toggle="tab">Change Password</a>
          </li>
        </ul>
        <div class="tab-content" style="padding-top:1.5em;">
          <div class="tab-pane <?php 
            if($this->session->flashdata('loggedinUserUpdateError1')):
              echo "";
            else:
              echo "active";
            endif;
           ?>" id="profile">
            <form>
              <p>
                <label for="fullname">Full Name:</label>
                <span><?php echo $userdetails[0]->first_name.' '.$userdetails[0]->last_name; ?></span>
              </p>
              <p>
                <label for="street">Street:</label>
                <span><?php echo $userdetails[0]->street; ?></span>
              </p>
              <p>
                <label for="address">Address:</label>
                <span><?php echo $userdetails[0]->address; ?></span>
              </p>
              <p>
                <label for="postcode">Post Code:</label>
                <span><?php echo $userdetails[0]->postcode; ?></span>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <label for="mobile">Mobile No.:</label>
                <span><?php echo $userdetails[0]->mobile_no; ?></span>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <label for="othertel">Other Tel No.:</label>
                <span><?php echo $userdetails[0]->other_no; ?></span>
              </p>
              <p>
                <label for="email">Email:</label>
                <span><?php echo $userdetails[0]->email; ?></span>
              </p>
              <p>
                <label for="username">Login username:</label>
                <span><?php echo $userdetails[0]->login_username; ?></span>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <label for="usertype">User Type:</label>
                <span><?php echo $userdetails[0]->user_type; ?></span>
              </p>
              <p>
                <label for="dateofjoin">Date of Join:</label>
                <span><?php echo $userdetails[0]->date_of_join; ?></span>
              </p>
            </form>
          </div>
          <div class="tab-pane <?php 
            if($this->session->flashdata('loggedinUserUpdateError1')):
              echo "active";
            else:
              echo "";
            endif;?>" id="editprofile">
            <!--Form starts here-->      
            <form action="<?php echo site_url('profile/loggedinUserUpdate'); ?>" method="post" role="form">
              <input type="hidden" name="loggedinUserId" value="<?php echo $userdetails[0]->user_id; ?>">
              <!--First name & last name-->      
              <div class="row">
                <div class="form-group col-lg-3">
                  <label for="nfirstname">
                    First Name :
                    <small>&nbsp;&nbsp;eg. john</small>
                  </label>
                  <input type="text" name="nfirstname" id="nfirstname" value="<?php echo $userdetails[0]->first_name; ?>" class="form-control" style="border-radius:0px;">
                  <span class="error"><?php echo form_error('nfirstname'); ?></span>
                </div>
                <div class="form-group col-lg-3">
                  <label for="nlastname">
                    Last Name :
                    <small>&nbsp;&nbsp;eg. smith</small>
                  </label>
                  <input type="text" name="nlastname" id="nlastname" value="<?php echo $userdetails[0]->last_name; ?>" class="form-control" style="border-radius:0px;">
                  <span class="error"><?php echo form_error('nfirstname'); ?></span>
                </div>
              </div>
              <!--first name and last name end here-->      
              <!--street and full address start-->      
              <div class="row">
                <div class="form-group col-lg-3">
                  <label for="nstreet">
                    Street :
                    <small>&nbsp;&nbsp;eg. lazimpat</small>
                  </label>
                  <input type="text" name="nstreet" id="nstreet" value="<?php echo $userdetails[0]->street; ?>" class="form-control" style="border-radius:0px;">
                  <span class="error"><?php echo form_error('nstreet'); ?></span>
                </div>
                <div class="form-group col-lg-5">
                  <label for="nfulladdress">
                    Full Address :
                    <small>&nbsp;&nbsp;eg. Lazimpat-2, Kathmandu, Nepal</small>
                  </label>
                  <input type="text" name="nfulladdress" id="nfulladdress" value="<?php echo $userdetails[0]->address; ?>" class="form-control" style="border-radius:0px;">
                  <span class="error"><?php echo form_error('nfulladdress'); ?></span>
                </div>
              </div>
              <!--street and full address end here-->      
              <!--mobile no. and other no. start-->      
              <div class="row">
                <div class="form-group col-lg-2">
                  <label for="npostcode">
                    Post Code:
                    <small>&nbsp;&nbsp;eg. 977</small>
                  </label>
                  <input type="number" name="npostcode" id="npostcode" value="<?php echo $userdetails[0]->postcode; ?>" class="form-control" style="border-radius:0px;">
                  <span class="error"><?php echo form_error('npostcode'); ?></span>
                </div>
                <div class="form-group col-lg-3">
                  <label for="nmobileno">
                    Mobile No.:
                    <small>&nbsp;&nbsp;eg. 980000000</small>
                  </label>
                  <input type="number" name="nmobileno" id="nmobileno" value="<?php echo $userdetails[0]->mobile_no; ?>" class="form-control" style="border-radius:0px;">
                  <span class="error"><?php echo form_error('nmobileno'); ?></span>
                </div>
                <div class="form-group col-lg-3">
                  <label for="nothertel">
                    Other Tel No.:
                    <small>&nbsp;&nbsp;eg. 014441561</small>
                  </label>
                  <input type="number" name="nothertel" id="nothertel" value="<?php echo $userdetails[0]->other_no; ?>" class="form-control" style="border-radius:0px;">
                  <span class="error"><?php echo form_error('nothertel'); ?></span>
                </div>
              </div>
              <!--mobile no. and other no. end here-->      
              <!--email start-->      
              <div class="row">
                <div class="form-group col-lg-4">
                  <label for="nemail">
                    Email :
                    <small>&nbsp;&nbsp;eg. gloomy.gurung@gmail.com</small>
                  </label>
                  <input type="text" name="nemail" id="nemail" value="<?php echo $userdetails[0]->email; ?>" class="form-control" style="border-radius:0px;">
                  <span class="error"><?php echo form_error('nemail'); ?></span>
                </div>
              </div>
              <!--email ends here-->      
              <!--username start-->      
              <div class="row">
                <div class="form-group col-lg-3">
                  <label for="username">Login username:</label>
                  <input type="text" name="nusername" id="nusername" value="<?php echo $userdetails[0]->login_username; ?>" class="form-control" style="border-radius:0px;">
                  <span class="error"><?php echo form_error('nusername'); ?></span>
                </div>
              </div>
              <!--username ends here-->      
              <!--submit and cancel button-->      
              <div class="row">
                <p>
                  <button type="submit" class="btn btn-primary" style="margin-left:1em; border-radius:0px;">Update</button>
                  <button type="reset" onclick="window.location.href='<?php echo site_url('profile'); ?>'" class="btn btn-danger" style="margin-top:0px; border-radius:0px;">Cancel</button>
                </p>
              </div>
              <!--submit and cancel button end here--> </form>
            <!--Form ends here--> </div>
          <div class="tab-pane" id="editpsw">
            <!--form to update password-->      
            <form action="" method="post" role="form" class="form-inline">
              <!--new password field-->      
              <div class="row">
                <div class="form-group col-lg-3">
                  <label for="username">New Password :</label>
                  <input type="text" name="nusername" id="nusername" value="" class="form-control" style="border-radius:0px;" placeholder="**************"></div>
              </div>
              <!--new password field ends here-->      
              <!--confirm password field-->      
              <div class="row">
                <div class="form-group col-lg-3">
                  <label for="Email">Confirm Password :</label>
                  <input type="email" name="nemail" id="nemail" value="" class="form-control" style="border-radius:0px;" placeholder="**************"></div>
              </div>
              <!--confirm password field ends here-->      
              <!--submit button-->      
              <div class="row">
                <p>
                  <button type="submit" class="btn btn-primary" style="margin-left:1em; margin-top:1em; border-radius:0px;">Update</button>
                </p>
              </div>
              <!--submit button ends here--> 
            </form>
            <!--password update form ends here--> 
          </div>
        </div>
      </div><!--maincon-->
      <hr>
