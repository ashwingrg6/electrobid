<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile_model extends CI_Model {

	//sets validation rules
  public $validation_loggedinUserUpdate1 = array(
            array(
              'field' => 'nfirstname',
              'label' => 'firstname',
              'rules' => 'required|trim|sanitize'
            ),
            array(
              'field' => 'nlastname',
              'label' => 'lastname',
              'rules' => 'required|trim|sanitize'
            ),
            array(
              'field' => 'nstreet',
              'label' => 'street',
              'rules' => 'required|trim|sanitize'
            ),
            array(
              'field' => 'nfulladdress',
              'label' => 'fulladdress',
              'rules' => 'required|trim|sanitize'
            ),
            array(
              'field' => 'npostcode',
              'label' => 'postcode',
              'rules' => 'required|trim|sanitize'
            ),
            array(
              'field' => 'nmobileno',
              'label' => 'mobileno',
              'rules' => 'required|trim|sanitize|min_length[10]|max_length[10]'
            ),
            array(
              'field' => 'nothertel',
              'label' => 'othertel',
              'rules' => 'trim|sanitize|min_length[9]'
            ),
            array(
              'field' => 'nusername',
              'label' => 'username',
              'rules' => 'required|trim|sanitize|min_length[5]|max_length[25]|is_unique[user.login_username]'
            )
  );


	//sets validation rules
  public $validation_loggedinUserUpdate2 = array(
            array(
              'field' => 'nfirstname',
              'label' => 'firstname',
              'rules' => 'required|trim|sanitize'
            ),
            array(
              'field' => 'nlastname',
              'label' => 'lastname',
              'rules' => 'required|trim|sanitize'
            ),
            array(
              'field' => 'nstreet',
              'label' => 'street',
              'rules' => 'required|trim|sanitize'
            ),
            array(
              'field' => 'nfulladdress',
              'label' => 'fulladdress',
              'rules' => 'required|trim|sanitize'
            ),
            array(
              'field' => 'npostcode',
              'label' => 'postcode',
              'rules' => 'required|trim|sanitize'
            ),
            array(
              'field' => 'nmobileno',
              'label' => 'mobileno',
              'rules' => 'required|trim|sanitize|min_length[10]|max_length[10]'
            ),
            array(
              'field' => 'nothertel',
              'label' => 'othertel',
              'rules' => 'trim|sanitize|min_length[9]'
            ),
            array(
              'field' => 'nemail',
              'label' => 'email',
              'rules' => 'required|trim|sanitize|valid_email|is_unique[user.email]'
            )
  );


	//sets validation rules
  public $validation_loggedinUserUpdate3 = array(
            array(
              'field' => 'nfirstname',
              'label' => 'firstname',
              'rules' => 'required|trim|sanitize'
            ),
            array(
              'field' => 'nlastname',
              'label' => 'lastname',
              'rules' => 'required|trim|sanitize'
            ),
            array(
              'field' => 'nstreet',
              'label' => 'street',
              'rules' => 'required|trim|sanitize'
            ),
            array(
              'field' => 'nfulladdress',
              'label' => 'fulladdress',
              'rules' => 'required|trim|sanitize'
            ),
            array(
              'field' => 'npostcode',
              'label' => 'postcode',
              'rules' => 'required|trim|sanitize'
            ),
            array(
              'field' => 'nmobileno',
              'label' => 'mobileno',
              'rules' => 'required|trim|sanitize|min_length[10]|max_length[10]'
            ),
            array(
              'field' => 'nothertel',
              'label' => 'othertel',
              'rules' => 'trim|sanitize|min_length[9]'
            )
  );

	//sets validation rules 
  public $validation_loggedinUserUpdate4 = array(
            array(
              'field' => 'nfirstname',
              'label' => 'firstname',
              'rules' => 'required|trim|sanitize'
            ),
            array(
              'field' => 'nlastname',
              'label' => 'lastname',
              'rules' => 'required|trim|sanitize'
            ),
            array(
              'field' => 'nstreet',
              'label' => 'street',
              'rules' => 'required|trim|sanitize'
            ),
            array(
              'field' => 'nfulladdress',
              'label' => 'fulladdress',
              'rules' => 'required|trim|sanitize'
            ),
            array(
              'field' => 'npostcode',
              'label' => 'postcode',
              'rules' => 'required|trim|sanitize'
            ),
            array(
              'field' => 'nmobileno',
              'label' => 'mobileno',
              'rules' => 'required|trim|sanitize|min_length[10]|max_length[10]'
            ),
            array(
              'field' => 'nothertel',
              'label' => 'othertel',
              'rules' => 'trim|sanitize|min_length[9]'
            ),
            array(
              'field' => 'nemail',
              'label' => 'email',
              'rules' => 'required|trim|sanitize|valid_email|is_unique[user.email]'
            ),
            array(
              'field' => 'nusername',
              'label' => 'username',
              'rules' => 'required|trim|sanitize|min_length[5]|max_length[25]|is_unique[user.login_username]'
            )
  );

}

/* End of file profile_model.php */
/* Location: .//C/xampp/htdocs/electrobid/_app/backend/profile/models/profile_model.php */