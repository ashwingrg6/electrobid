<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Item_model extends CI_Model {

	//sets validation rules for new Item
  public $validation_newItem = array(
            array(
              'field' => 'itemName',
              'label' => 'Item Name',
              'rules' => 'required|trim|sanitize'
            ),
            array(
              'field' => 'itemPrice',
              'label' => 'Item Price',
              'rules' => 'required|trim|sanitize'
            ),
            array(
              'field' => 'startPrice',
              'label' => 'Start Price',
              'rules' => 'required|trim|sanitize'
            ),
            array(
              'field' => 'endDate',
              'label' => 'Bid End Date',
              'rules' => 'required|trim|sanitize'
            ),
            array(
              'field' => 'category',
              'label' => 'Category',
              'rules' => 'required|trim|sanitize'
            ),
            array(
              'field' => 'product',
              'label' => 'Product',
              'rules' => 'required|trim|sanitize'
            ),
             array(
              'field' => 'itemImage',
              'label' => 'Product',
              'rules' => 'trim|sanitize'
            )
  );

  /**
   *
   * @return 
   */
  public function insertItem($insertData){
    $query = $this->db->insert('item',$insertData);
    if($query){
      return $this->db->insert_id();
    }
  }

  /**
   *
   * @return 
   */
  public function insertAuction($insertData){
    $query = $this->db->insert('auction', $insertData);
    if($query){
      return true;
    }
  }

    /**
   *
   * @return 
   */
  public function updateItem($updateId, $updateData){
    $query = $this->db->where('item_id', $updateId)
                      ->update('item',$updateData);
    if($query){
      return true;
    }
  }

  /**
   *
   * @return 
   */
  public function updateAuction($updateId, $updateData){
    $query = $this->db->where('auction_id', $updateId)
                      ->update('auction', $updateData);
    if($query){
      return true;
    }
  }

  /**
   *
   * @return 
   */
  public function getItemsByUserId($userid){
    $query = $this->db->select('*')
                      ->where('item.user_id', $userid)
                      ->from('item')
                      // ->where('auction.user_id', $userid)
                      ->join('category', "item.category_id = category.category_id")
                      ->join('product', "item.product_id = product.product_id")
                      ->join('auction', "auction.item_id = item.item_id")
                      ->order_by('item_timestamp', 'DESC')
                      ->get();
    return $query->result();
  }

  /**
   *
   * @return 
   */
  public function deleteItem($itemId){
    $queryD = $this->db->select('item_image')
              ->where('item_id', $itemId)
              ->from('item')
              ->get()->result();
    $itemImage = $queryD[0]->item_image;
    unlink($_SERVER['DOCUMENT_ROOT'].'/electrobid/uploads/'.$itemImage);
    // unlink(URL1."uploads/".$itemImage);
    $query = $this->db->delete('item', array('item_id' => $itemId));
    if($query){
      return true;
    }
  }

   /**
   *
   * @return 
   */
  public function getItemById($itemId){
    $query = $this->db->select('*')
              ->where('item_id', $itemId)
              ->from('item')
              ->join('category', "item.category_id = category.category_id")
              ->join('product', 'item.product_id = product.product_id')
              ->get();
    if($query){
      return $query->result();
    }
  }

  /**
   *
   * @return 
   */
  public function getAuctionByItem($itemId){
    $query = $this->db->get_where('auction', array('item_id' => $itemId));
    return $query->result();
  }

}

/* End of file item_model.php */
/* Location: .//C/xampp/htdocs/electrobid/_app/backend/items/models/item_model.php */