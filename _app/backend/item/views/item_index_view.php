      <!--<p class="well lead">asdf</p>
      <p class="alert alert-dismissable alert-danger">asfadsfas</p>
      <p class="alert alert-dismissable alert-danger">asdfadfda</p>
      -->
      <h2>All Items</h2><hr>
      <?php if($this->session->flashdata('itemDeleted')){ ?>
      <!--notify msg-->
      <div class="alert alert-success alert-dismissable" style="padding:8px; border-radius:0px;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo $this->session->flashdata('itemDeleted'); ?>
      </div><!--msg notify ends-->
      <?php } ?>
      <?php if($this->session->flashdata('itemUpdated')){ ?>
      <!--notify msg-->
      <div class="alert alert-success alert-dismissable" style="padding:8px; border-radius:0px;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo $this->session->flashdata('itemUpdated'); ?>
      </div><!--msg notify ends-->
      <?php } ?>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 maincontent" style="padding-left: 0px;">
        <!--currently available auctions-->
        <div class="panel panel-default">
          <div class="panel-heading"><strong>All items uploaded by you.</strong></div>
          <!--panel-heading ends-->
          <div class="panel-body">
            <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover" id="dataTables-example2">
                <thead>
                  <tr>
                    <th width="60">S-N</th>
                    <th width="100">Category</th>
                    <th width="100">Product</th>
                    <th width="200">Item Name</th>
                    <th width="110">Image</th>
                    <th width="100">Price</th>
                    <th width="100">Item Status</th>
                    <th width="100">Auction Status</th>
                    <th width="100">Bid Start Price</th>
                    <th width="100">Bid Close Date</th>
                    <th width="120">Action</th>
                  </tr>
                </thead>
                <tbody>
                <?php 
                  $sn = 1;
                  // print_r($allitems);die();
                  foreach ($allitems as $key => $value):
                 ?>
                  <tr class="odd">
                    <td style="text-align:center;"><?php echo $sn; ?></td>
                    <td><?php echo $value->category_name; ?></td>
                    <td><?php echo $value->product_name; ?></td>
                    <td><?php echo $value->item_name; ?></td>
                    <td><img src="<?php echo URL1.'uploads/'.$value->item_image; ?>" alt="" height="40" width="60"></td>
                    <td><?php echo $value->item_price; ?></td>
                    <td><?php echo $value->item_status; ?></td>
                    <td><?php echo $value->auction_status; ?></td>
                    <td><?php echo $value->start_price; ?></td>
                    <td class="center"><?php echo $value->close_date; ?></td>
                    <td class="center" style="margin:0px; padding:5px 0px 0px 7px;">
                    <?php if($value->item_status != 'sold'){ ?>
                      <button class="btn btn-primary btn-sm" onclick="window.location.href='<?php echo site_url('item/add').'/'.$value->item_id; ?>';">Edit</button>
                    <?php } ?>
                      <button class="btn btn-danger btn-sm" onclick="window.location.href='<?php echo site_url('item/delete').'/'.$value->item_id; ?>';">Delete</button>
                    </td>
                  </tr>
                  <?php $sn++; endforeach; ?>
                </tbody>
              </table>
            </div>
            <!-- table-responsive ends -->
          </div>
          <!-- /.panel-body -->
        </div><!--currently available auctions-->
      </div><!--maincon-->
      <hr>
