      <!--<p class="well lead">asdf</p>
      <p class="alert alert-dismissable alert-danger">asfadsfas</p>
      <p class="alert alert-dismissable alert-danger">asdfadfda</p>
      -->
      <h2>Add New Item</h2>
      <hr>
      <?php if($this->session->flashdata('itemAdded')){ ?>
      <!--notify msg-->
      <div class="alert alert-success alert-dismissable" style="padding:8px; border-radius:0px;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo $this->session->flashdata('itemAdded'); ?>
      </div><!--msg notify ends-->
      <?php } ?>
      <!--form to add new item-->
      <div class="md-col-8" >
        <form method="post" name="addItem" action="" enctype="multipart/form-data">
          <!--Item name-->
          <div class="form-group" >
            <label for="Item Name" class="control-label col-md-2" style="text-align:left;">Item Name:</label>
            <div class="col-md-3" style="padding:0px;">
              <?php if(isset($editInfo1)){ ?>
              <input type="hidden" name="itemId" value="<?php echo $editInfo1[0]->item_id; ?>"><?php } ?>
              <input type="text" name="itemName" id="itemName" class="form-control" style="border-radius:0px;" placeholder="Item Name" value="<?php if(isset($editInfo1)){ echo $editInfo1[0]->item_name; } else{ echo set_value('itemName'); } ?>">
              <span class="error" >
                <?php echo form_error('itemName'); ?>
              </span>
            </div>
          </div><br><br><br><!--Item Name ends here-->
          <!--Item Price-->
          <div class="form-group" >
            <label for="Item Price" class="control-label col-md-2" style="text-align:left;">Item Price:</label>
            <div class="col-md-3" style="padding:0px;">
              <input type="number" name="itemPrice" id="itemPrice" class="form-control" style="border-radius:0px;" placeholder="Item Price" value="<?php if(isset($editInfo1)){echo $editInfo1[0]->item_price;} else{echo set_value('itemPrice');} ?>">
              <span class="error" >
                <?php echo form_error('itemPrice'); ?>
              </span>
            </div>
          </div><br><br><!--Item Price ends here-->
          <!--Bid Info-->
          <div class="form-group" >
            <label for="Start Price" class="control-label col-md-2" style="text-align:left;">Bid Start Price:</label>
            <div class="col-md-3" style="padding:0px;">
              <input type="number" name="startPrice" id="startPrice" class="form-control" style="border-radius:0px;" placeholder="Bid Start Price" value="<?php if(isset($editInfo2)){echo $editInfo2[0]->start_price;} else{ echo set_value('startPrice');} ?>">
              <span class="error" >
                <?php echo form_error('startPrice'); ?>
              </span>
            </div>
            <label for="End Date" class="control-label col-md-1" style="text-align:left;">Bid End Date:</label>
            <div class="col-md-3" style="padding:0px;">
              <input type="date" name="endDate" id="endDate" class="form-control" style="border-radius:0px;" placeholder="Item Price" value="<?php if(isset($editInfo2)){echo $editInfo2[0]->close_date; } else{echo set_value('endDate');} ?>">
              <span class="error" >
                <?php echo form_error('endDate'); ?>
              </span>
            </div>
          </div><br><br><!--Bid info ends here-->
          <!--Item Image-->
          <div class="form-group" >
            <label for="Item Image" class="control-label col-md-2" style="text-align:left;">Choose Item Image:</label>
            <div class="col-md-3" style="padding:0px;">
              <input type="file" name="itemImage" id="itemImage" class="form-control" style="border-radius:0px;" <?php if(!isset($editInfo1)){ ?>required="required"<?php } ?> value="<?php if(isset($editInfo1)){echo $editInfo1[0]->item_image;} ?>">

            </div>
            <div class="col-md-5">
              <span class="error" >
                <?php
                if(isset($fileError)):
                  echo $fileError['error'];
                endif;
                ?>
              </span>
            </div>
            <?php if(isset($editInfo1)){ ?>
            <!--Old Image-->
            <div class="clearfix"></div>
            <div class="col-md-2">Current Image:</div>
            <div class="col-md-3" style="height:200px; border:1px solid #aeaeae; margin-top:5px;">
              <img src="<?php echo URL1.'uploads/'.$editInfo1[0]->item_image; ?>" alt="">
            </div>
            <div class="clearfix"></div>
            <!--Old Image-->
            <?php } ?>
          </div><br><br><!--Item Image ends here-->
          <!--Choose Category-->
          <div class="form-group" >
            <label for="Choose Category" class="control-label col-md-2" style="text-align:left;">Choose Category:</label>
            <div class="col-md-3" style="padding:0px;">
              <select name="category" id="category" class="form-control" style="border-radius:0px;">
                <option value="">Please Select</option>
                <?php foreach ($allcategories as $key => $value) { ?>
                <option value="<?php echo $value->category_id; ?>" <?php if(isset($editInfo1)){ if($editInfo1[0]->category_name == $value->category_name){ echo "selected='selected'"; } } ?>><?php echo $value->category_name ?></option>
                <?php } ?>
              </select>
              <span class="error" >
                <?php echo form_error('category'); ?>
              </span>
            </div>
            <label for="Choose Product" class="control-label col-md-1" style="text-align:left;">Choose Product:</label>
            <div class="col-md-3" style="padding:0px;">
              <select name="product" id="product" class="form-control" style="border-radius:0px;">
                <option value="">Please Select</option>
                <?php foreach ($allproducts as $key => $value2) { ?>
                <option value="<?php echo $value2->product_id; ?>" <?php if(isset($editInfo1)){ if($editInfo1[0]->product_name == $value2->product_name){ echo "selected='selected'"; } } ?>><?php echo $value2->product_name; ?></option>
                <?php } ?>
              </select>
              <span class="error" >
                <?php echo form_error('product'); ?>
              </span>
            </div>
          </div><br><br><br><!--Choose category ends here-->
          <!--Item Description-->
          <div class="form-group" >
            <label for="Product Description" class="control-label col-md-2" style="text-align:left;">Item Description:</label>
            <div class="col-md-8" style="padding:0px;">
              <textarea name="itemDescription" cols="" rows="2" class="form-control" style="border-radius:0px;" id="itemDescriptioin"><?php if(isset($editInfo1)){ echo $editInfo1[0]->item_description; } ?></textarea>
            </div><div class="clearfix"></div>
          </div><!--Item Description ends here--><br>
          <div class="form-group">
            <label for="" class="control-label col-md-2"></label>
            <button type="submit" class="btn btn-primary" style="border-radius:0px; margin-left:4px;"><?php if($this->uri->segment(3)){ echo 'UPDATE'; } else { echo 'ADD NEW'; } ?></button>
            <button type="reset" class="btn btn-danger" style="border-radius:0px;" onclick="window.location.href='<?php echo site_url('item'); ?>'">CANCEL</button>
          </div>
        </form>
      </div><!--new item ends here-->
      <hr>
<script>
  $('#category').on('change',function(){
    var catid = $(this).val();
    var data='categoryid='+catid;
    var url = "<?php echo site_url('item/ajaxfilter'); ?>";
    $.post(url,data,function(res){
      $('#product').empty().html(res);
    });
  });
  CKEDITOR.replace('itemDescriptioin');
</script>