<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Item extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('item_model','item');
	}

	public function index(){
		$userid = $this->session->userdata('userid');
		$data['allitems'] = $this->item->getItemsByUserId($userid);
		$data['userdetails'] = Modules::run('users/dataforHeader');
		$this->template->load('backend/template/main', 'item_index_view', $data);
	}

	/**
	 *
	 * @return 
	 */
	public function add(){
		if($_POST):
			$itemId = $this->input->post('itemId', TRUE);
			$this->form_validation->set_rules($this->item->validation_newItem);
			if($this->form_validation->run() == TRUE){
				if($itemId){
					if($this->input->post('itemImage', TRUE) == null){
						$item_name = $this->input->post('itemName', TRUE);
						$item_price = $this->input->post('itemPrice', TRUE);
						$start_price = $this->input->post('startPrice', TRUE);
						$end_date = $this->input->post('endDate', TRUE);
						$category = $this->input->post('category', TRUE);
						$product = $this->input->post('product', TRUE);
						$item_description = $this->input->post('itemDescription', TRUE);
						$current_date = Modules::run('users/currentDate');
						$user_id = $this->session->userdata('userid');
						$now = now();
						$updateData1 = array(
						'category_id' => $category,
						'product_id' => $product,
						'user_id' => $user_id,
						'item_name' => $item_name,
						'item_price' => $item_price,
						'item_description' => $item_description,
						);
						$updateData2 = array(
							'item_id' => $item_id,
							'user_id' => $user_id,
							'close_date' => $end_date,
							'start_price' => $start_price,
						);
						$flag1 = $this->item->updateItem($itemId, $updateData1);
						if($flag1){
							$flag2 = $this->item->updateAuction($itemId, $updateData2);
							if($flag2){
								$this->session->set_flashdata('itemUpdated', 'Success: Selected item has been updated successfully.');
								redirect('item');
							}
						}
					}
					else{
						$config['upload_path'] = '../uploads/';
						$config['allowed_types'] = 'gif|jpg|png';
						$config['max_size']	= '400';
						$config['max_width']  = '1024';
						$config['max_height']  = '768';
						$config['encrypt_name'] = true;
						$this->load->library('upload', $config);
						if ( ! $this->upload->do_upload('itemImage')){
							redirect(current_url());
						}
						else {
							$uploadFlag = array('upload_data' => $this->upload->data());
							// print_r($uploadFlag);
							$item_image = $uploadFlag['upload_data']['file_name'];
							$item_name = $this->input->post('itemName', TRUE);
							$item_price = $this->input->post('itemPrice', TRUE);
							$start_price = $this->input->post('startPrice', TRUE);
							$end_date = $this->input->post('endDate', TRUE);
							$category = $this->input->post('category', TRUE);
							$product = $this->input->post('product', TRUE);
							$item_description = $this->input->post('itemDescription', TRUE);
							$current_date = Modules::run('users/currentDate');
							$user_id = $this->session->userdata('userid');
							$now = now();
							$updateData1 = array(
							'category_id' => $category,
							'product_id' => $product,
							'user_id' => $user_id,
							'item_name' => $item_name,
							'item_price' => $item_price,
							'item_image' => $item_image,
							'item_description' => $item_description,
							);
							$updateData2 = array(
								'item_id' => $item_id,
								'user_id' => $user_id,
								'close_date' => $end_date,
								'start_price' => $start_price,
							);
							$flag1 = $this->item->updateItem($itemId, $updateData1);
							if($flag1){
								$flag2 = $this->item->updateAuction($itemId, $updateData2);
								if($flag2){
									$this->session->set_flashdata('itemUpdated', 'Success: Selected item has been updated successfully.');
									redirect('item');
								}
							}
						}}
					}
				else{
					$config['upload_path'] = '../uploads/';
					$config['allowed_types'] = 'gif|jpg|png';
					$config['max_size']	= '400';
					$config['max_width']  = '1024';
					$config['max_height']  = '768';
					$config['encrypt_name'] = true;
					$this->load->library('upload', $config);
					if ( ! $this->upload->do_upload('itemImage')){
						$data['fileError'] = array('error' => $this->upload->display_errors());
						$data['allproducts'] = Modules::run('product/returnAllProducts');
						$data['allcategories'] = Modules::run('category/returnAllCategories');
						$data['userdetails'] = Modules::run('users/dataforHeader');
						$this->template->load('backend/template/main', 'item_add_view', $data);
					}
					else {
						$uploadFlag = array('upload_data' => $this->upload->data());
						// print_r($uploadFlag);
						$item_image = $uploadFlag['upload_data']['file_name'];
						$item_name = $this->input->post('itemName', TRUE);
						$item_price = $this->input->post('itemPrice', TRUE);
						$start_price = $this->input->post('startPrice', TRUE);
						$end_date = $this->input->post('endDate', TRUE);
						$category = $this->input->post('category', TRUE);
						$product = $this->input->post('product', TRUE);
						$item_description = $this->input->post('itemDescription', TRUE);
						$current_date = Modules::run('users/currentDate');
						$user_id = $this->session->userdata('userid');
						$now = now();
						$insertData1 = array(
							'category_id' => $category,
							'product_id' => $product,
							'user_id' => $user_id,
							'item_name' => $item_name,
							'item_price' => $item_price,
							'item_image' => $item_image,
							'item_description' => $item_description,
							'item_timestamp' => $now
						);
						$item_id = $this->item->insertItem($insertData1);
						$insertData2 = array(
							'item_id' => $item_id,
							'user_id' => $user_id,
							'create_date' => $current_date,
							'close_date' => $end_date,
							'start_price' => $start_price,
							'auction_timestamp' => $now
						);
						if($item_id){
							$auctionFlag = $this->item->insertAuction($insertData2);
							if($auctionFlag){
								$this->session->set_flashdata('itemAdded', 'Success: New item has been added successfully.');
								redirect(current_url());
							}
						}
					}
				}
			}
			else{
				$this->form_validation->set_error_delimiters('','');
				$this->addCommonFunction();
			}
		else:
			if($this->uri->segment(3)){
				$editId = $this->uri->segment(3);
				$data['editInfo1'] = $this->item->getItemById($editId);
				$data['editInfo2'] = $this->item->getAuctionByItem($editId);
			}
			$data['allproducts'] = Modules::run('product/returnAllProducts');
			$data['allcategories'] = Modules::run('category/returnAllCategories');
			$data['userdetails'] = Modules::run('users/dataforHeader');
			$this->template->load('backend/template/main', 'item_add_view', $data);
		endif;
	}

	/**
	 *
	 * @return 
	 */
	public function ajaxfilter(){
		$categoryId = $this->input->post('categoryid');
		$data['products'] = Modules::run('product/returnProductByCategory', $categoryId);
		$this->load->view('item/product_filter',$data);
	}

	/**
	 *
	 * @return 
	 */
	public function addCommonFunction(){
		$data['allproducts'] = Modules::run('product/returnAllProducts');
		$data['allcategories'] = Modules::run('category/returnAllCategories');
		$data['userdetails'] = Modules::run('users/dataforHeader');
		$this->template->load('backend/template/main', 'item_add_view', $data);
	}

	/**
	 *
	 * @return 
	 */
	public function delete($itemId){
		$deleteFlag = $this->item->deleteItem($itemId);
		if($deleteFlag)
			$this->session->set_flashdata('itemDeleted', 'Success: Selected item has been deleted successfully.');
			redirect('item');
	}
}

/* End of file item.php */
/* Location: .//C/xampp/htdocs/electrobid/_app/backend/items/controllers/item.php */