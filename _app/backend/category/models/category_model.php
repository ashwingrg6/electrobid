<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category_model extends CI_Model {

	//sets validation rules for new Category
  public $validation_newCategory = array(
            array(
              'field' => 'cname',
              'label' => 'category name',
              'rules' => 'required|trim|sanitize|is_unique[category.category_name]'
            )
  );

  //sets validation rules for update Category
  public $validation_updateCategory = array(
            array(
              'field' => 'cname',
              'label' => 'category name',
              'rules' => 'required|trim|sanitize'
            )
  );

  /**
   *
   * @return 
   */
	public function allCategories(){
		$query = $this->db->get('category');
		return $query->result();
	}

	/**
	 *
	 * @return 
	 */
	public function insertCategory($insertData){
		$query = $this->db->insert('category', $insertData);
		if($query){
			return true;
		}
	}

		/**
	 *
	 * @return 
	 */
	public function deleteCategory($categoryId){
		$query = $this->db->delete('category', array('category_id' => $categoryId));
		if($query){
			return true;
		}
	}


	/**
	 *
	 * @return 
	 */
	public function getCategoryById($categoryId){
		$query = $this->db->get_where('category', array('category_id' => $categoryId));
		if($query){
			return $query->result();
		}
	}

	/**
	 *
	 * @return 
	 */
	public function updateCategory($categoryId, $updateData){
		$this->db->where('category_id', $categoryId);
    $this->db->update('category', $updateData);
		return true;
	}

}

/* End of file category_model.php */
/* Location: .//C/xampp/htdocs/electrobid/_app/backend/categories/models/category_model.php */