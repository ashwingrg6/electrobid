<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category extends MX_Controller {

	/**************** constructor function ************/
	public function __construct(){
		parent::__construct();
		// Modules::run('users/checkLoginSession');
		$this->load->model('category_model','category');
	}
	/************************ constructor ends ****************************/

	/**
	 *
	 * @return 
	 */
	public function index(){
		if($_POST):
			$categoryName = $this->input->post('cname', TRUE);
			$categoryDescription = $this->input->post('cdescription', TRUE);
			$this->form_validation->set_rules($this->category->validation_newCategory);
			if($this->form_validation->run() == TRUE):
				$insertData = array(
					'category_name' => $categoryName,
					'category_description' => $categoryDescription
				);
				$insertFlag = $this->category->insertCategory($insertData);
				if($insertFlag){
					$this->session->set_flashdata('categoryAdded', 'Success: New Category has been added successfully.');
					redirect('category');
				};
			else:
				$this->form_validation->set_error_delimiters('','');
				$data['allcategories'] = $this->category->allCategories();
				$data['userdetails'] = modules::run('users/dataforHeader');
				$this->template->load('backend/template/main', 'category_index_view', $data);
			endif;
		else:
			$data['allcategories'] = $this->category->allCategories();
			$data['userdetails'] = modules::run('users/dataforHeader');
			$this->template->load('backend/template/main', 'category_index_view', $data);
		endif;
	}

	/**
	 *
	 * @return 
	 */
	public function delete($categoryId){
		$deleteFlag = $this->category->deleteCategory($categoryId);
		if($deleteFlag){
			$this->session->set_flashdata('categoryDeleted', 'Success: Selected Category has been deleted successfully.');
			redirect('category');
		}
	}

	/**
	 *
	 * @return 
	 */
	public function edit(){
		$categoryId = $this->uri->segment(3);
		if($_POST){
			$category_id = $this->input->post('categoryid', TRUE);
			$categoryName = $this->input->post('cname', TRUE);
			$categoryDescription = $this->input->post('cdescription', TRUE);
			$this->form_validation->set_rules($this->category->validation_updateCategory);
			if($this->form_validation->run() == TRUE):
				$updateData = array(
					'category_name' => $categoryName,
					'category_description' => $categoryDescription
				);
				$updateFlag = $this->category->updateCategory($category_id, $updateData);
				if($updateFlag){
					$this->session->set_flashdata('categoryUpdated', 'Success: Selected Category has been updated successfully.');
					redirect('category');
				};
			else:
				$this->form_validation->set_error_delimiters('','');
				$data['allcategories'] = $this->category->allCategories();
				$data['userdetails'] = modules::run('users/dataforHeader');
				$this->template->load('backend/template/main', 'category_index_view', $data);
			endif;
		}
		$data['editInfo'] = $this->category->getCategoryById($categoryId);
		$data['allcategories'] = $this->category->allCategories();
		$data['userdetails'] = modules::run('users/dataforHeader');
		$this->template->load('backend/template/main', 'category_edit_view', $data);
	}


	/**
	 *
	 * @return 
	 */
	public function returnAllCategories(){
		return $this->category->allCategories();

	}

}

/* End of file category.php */
/* Location: .//C/xampp/htdocs/electrobid/_app/backend/categories/controllers/category.php */