      <!--<p class="well lead">asdf</p>
      <p class="alert alert-dismissable alert-danger">asfadsfas</p>
      <p class="alert alert-dismissable alert-danger">asdfadfda</p>
      -->
      <h2>Edit Category</h2><hr>
      <!--form to add new category-->
      <div class="md-col-8">
        <form method="post" name="newcategory" action="<?=site_url('category/edit'); ?>">
          <div class="form-group" >
            <label for="Category Name" class="control-label col-md-1" style="text-align:left;">Name :</label>
            <div class="col-md-3" style="padding:0px;">
              <input type="hidden" name="categoryid" value="<?php echo $editInfo[0]->category_id; ?>">
              <input type="text" class="form-control" name="cname" id="cname" value="<?php if(isset($editInfo)){ echo $editInfo[0]->category_name; } ?>" placeholder="Enter Category Name" style="border-radius:0px;">
            </div>
            <span class="error">
              <?php echo form_error('cname'); ?>
            </span>
            <br><br>
          </div>
          <div class="form-group" >
            <label for="Category Description" class="control-label col-md-1" style="text-align:left;">Description:</label>
            <div class="col-md-4" style="padding:0px;">
              <textarea name="cdescription" id="" cols="" rows="2" class="form-control" style="border-radius:0px;" placeholder="Enter Description..."><?php if(isset($editInfo)){ echo $editInfo[0]->category_description; } ?></textarea>
            </div>
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary" style="border-radius:0px; margin-left:4px;">UPDATE</button>
            <button type="reset" class="btn btn-danger" style="border-radius:0px;" onclick="window.location.href='<?php echo site_url('category'); ?>'">CANCEL</button>
          </div>
        </form>
      </div><!--new category ends here-->

