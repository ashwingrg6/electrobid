      <!--<p class="well lead">asdf</p>
      <p class="alert alert-dismissable alert-danger">asfadsfas</p>
      <p class="alert alert-dismissable alert-danger">asdfadfda</p>
      -->
      <h2>Categories</h2><hr>
      <?php if($this->session->flashdata('categoryAdded')){ ?>
      <!--notify msg-->
      <div class="alert alert-success alert-dismissable" style="padding:8px; border-radius:0px;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo $this->session->flashdata('categoryAdded'); ?>
      </div><!--msg notify ends-->
      <?php } ?>
      <?php if($this->session->flashdata('categoryUpdated')){ ?>
      <!--notify msg-->
      <div class="alert alert-success alert-dismissable" style="padding:8px; border-radius:0px;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo $this->session->flashdata('categoryUpdated'); ?>
      </div><!--msg notify ends-->
      <?php } ?>
      <!--form to add new category-->
      <div class="md-col-8">
        <form method="post" name="newcategory" action="<?=current_url(); ?>">
          <div class="form-group" >
            <label for="Category Name" class="control-label col-md-1" style="text-align:left;">Name :</label>
            <div class="col-md-3" style="padding:0px;">
              <input type="text" class="form-control" name="cname" id="cname" value="" placeholder="Enter Category Name" style="border-radius:0px;">
            </div>
            <span class="error">
              <?php echo form_error('cname'); ?>
            </span>
            <br><br>
          </div>
          <div class="form-group" >
            <label for="Category Description" class="control-label col-md-1" style="text-align:left;">Description:</label>
            <div class="col-md-4" style="padding:0px;">
              <textarea name="cdescription" id="" cols="" rows="2" class="form-control" style="border-radius:0px;" placeholder="Enter Description..."></textarea>
            </div>
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary" style="border-radius:0px; margin-left:4px;">ADD</button>
            <button type="reset" class="btn btn-danger" style="border-radius:0px;">CANCEL</button>
          </div>
        </form>
      </div><!--new category ends here-->
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 maincontent" style="padding:10px 0; border-top:1px solid #dddddd; margin-top:10px;">
        <?php if($this->session->flashdata('categoryDeleted')){ ?>
        <!--notify msg-->
        <div class="alert alert-success alert-dismissable" style="padding:8px; border-radius:0px;">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <?php echo $this->session->flashdata('categoryDeleted'); ?>
        </div><!--msg notify ends-->
        <?php } ?>
        <!--all categories-->
        <div class="panel panel-default">
          <div class="panel-heading" ><strong>All Categories</strong></div>
          <!--panel-heading ends-->
          <div class="panel-body">
            <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover" id="dataTables-example2">
                <thead>
                  <tr>
                    <th width="100">S-N</th>
                    <th>Category Name</th>
                    <th>Description</th>
                    <th width="130">Action</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                $sn = 1;
                foreach ($allcategories as $key => $value):
                ?>
                  <tr>
                    <td style="text-align:center;"><?php echo $sn; ?></td>
                    <td><?php echo $value->category_name; ?></td>
                    <td><?php echo $value->category_description; ?></td>
                    <td class="center" style="margin:0px; padding:5px 0px 0px 7px;">
                      <button class="btn btn-primary btn-sm" onclick="window.location.href='<?php echo site_url('category/edit').'/'.$value->category_id; ?>';">Edit</button>
                      <button class="btn btn-danger btn-sm" onclick="window.location.href='<?php echo site_url('category/delete').'/'.$value->category_id; ?>';">Delete</button>
                    </td>
                  </tr>
                <?php $sn++;endforeach; ?>
                </tbody>
              </table>
            </div>
            <!-- table-responsive ends -->
          </div>
          <!-- /.panel-body -->
        </div><!--all categories-->
      </div><!--maincon-->
      <hr>
