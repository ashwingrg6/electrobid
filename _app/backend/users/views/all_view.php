<?php
$loggedinUserId = $this->session->userdata('userid');
$usertype = null;
if(isset($userType)):
	if($userType == 'admin'):
		$usertype = 'admin';
	else:
		$usertype = 'user';
	endif;
endif;
?>
<h2>
<?php 
	if($this->uri->segment(2) == 'filter'):
		echo "All users :: Filtered";
	else:
		echo "All users";
	endif;
?>
</h2><hr>
<div class="col-8">
	<form method="post" name="filterUser" action="<?=site_url('users/filter'); ?>">
		<p>
			<span class="col-lg-1" style="padding:0px;">
				<label for=""><strong>Filter By:</strong></label>
			</span>
			<span class="col-lg-2">
				<select name="usertype" id="usertype" class="form-control" style="border-radius:0px;">
					<option value="all" <?php if($usertype == null){ echo "selected='selected'"; } ?>>All</option>
					<option value="admin" <?php if($usertype == 'admin'){ echo "selected='selected'"; } ?>>Admins</option>
					<option value="user" <?php if($usertype == 'user'){ echo "selected='selected'"; } ?>>Users</option>
				</select>
			</span>
			<span class="col-lg-1">
				<button type="submit" class="btn btn-primary btn-primary" value="" style="border-radius:0px;">Filter</button>
			</span>
		</p>
	</form>
</div><br><br>
<?php if($this->session->flashdata('deletedUserMsg')){ ?>
<!--notify msg-->
<div class="alert alert-success alert-dismissable" style="padding:8px; border-radius:0px;">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <?php echo $this->session->flashdata('deletedUserMsg'); ?>
</div><!--msg notify ends-->
<?php } ?>
<div class="col-lg-12" style="margin:0px; padding:0px;">
	<div class="panel panel-default">
		<div class="panel-heading"><strong>All registered users, availabel in database</strong></div>
		<!--panel-heading ends-->
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover" id="dataTables-example">
					<thead>
						<tr>
							<th width="60">S-N</th>
							<th width="130">Full Name</th>
							<th width="200">Email</th>
							<th width="130">Username</th>
							<th width="200">Full Address</th>
							<th width="50">Mobile No.</th>
							<th width="115">Other Tel No.</th>
							<th width="50">Type</th>
							<th width="125">Action</th>
						</tr>
					</thead>
					<tbody>
					<?php
						$sn = 1;
						foreach ($allusers as $key => $value):
					?>
						<tr class="odd">
							<td style="text-align:center;"><?=$sn; ?></td>
							<td><?=$value->first_name.' '.$value->last_name; ?></td>
							<td><?=$value->email; ?></td>
							<td><?=$value->login_username; ?></td>
							<td><?=$value->address; ?></td>
							<td><?=$value->mobile_no; ?></td>
							<td><?=$value->other_no; ?></td>
							<td class="center"><?=$value->user_type; ?></td>
							<td class="center" style="margin:0px; padding:5px 0px 0px 7px;">
							<?php if($value->email != 'admin@admin.com'){ 
									if($value->user_id != $loggedinUserId){
							?>
								<button class="btn btn-danger btn-sm" onclick="window.location.href='<?php echo site_url('users/delete').'/'.$value->user_id; ?>';">Delete</button>
							<?php }} ?>
							</td>
						</tr>
						<?php $sn++; endforeach;  ?>
					</tbody>
				</table>
			</div>
			<!-- table-responsive ends -->
		</div>
		<!-- /.panel-body -->
	</div>
	<!--panel ends-->
</div>