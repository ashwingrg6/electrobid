      <!--<p class="well lead">asdf</p>
      <p class="alert alert-dismissable alert-danger">asfadsfas</p>
      <p class="alert alert-dismissable alert-danger">asdfadfda</p>
      -->
      <h2>Add new Admin</h2>
      <hr>
      <p>
        Please fill up the form and click submit button to register new admin
      </p>
      <?php if($this->session->flashdata('adminAdded')){ ?>
      <!--notify msg-->
      <div class="alert alert-success alert-dismissable" style="padding:8px; border-radius:0px;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo $this->session->flashdata('adminAdded'); ?>
      </div><!--msg notify ends-->
      <?php } ?>
      <!--notify msg-->
      <!--<div class="alert alert-danger alert-dismissable" style="padding:8px; border-radius:0px;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        asdfasdfdad
      </div>--><!--msg notify ends-->
      <!--Form starts here-->
      <form action="<?php echo site_url('users/add'); ?>" method="post" role="form">
        <!--First name & last name-->
        <div class="row">
          <div class="form-group col-lg-3">
            <label for="firstname">
              First Name :
              <small>&nbsp;&nbsp;eg. john</small>
            </label>
            <input type="text" name="firstname" id="firstname" value="<?php echo set_value('firstname');?>" class="form-control" style="border-radius:0px;" placeholder="First Name">
            <span class="error">
              <?php echo form_error('firstname'); ?>
            </span>
          </div><!--first name ends-->
          <div class="form-group col-lg-3">
            <label for="lastname">
              Last Name :
              <small>&nbsp;&nbsp;eg. smith</small>
            </label>
            <input type="text" name="lastname" id="lastname" value="<?php echo set_value('lastname');?>" class="form-control" style="border-radius:0px;" placeholder="Last Name">
            <span class="error">
              <?php echo form_error('lastname'); ?>
            </span>
          </div>
        </div>
        <!--first name and last name end here-->      
        <!--street and full address start-->      
        <div class="row">
          <div class="form-group col-lg-3">
            <label for="street">
              Street :
              <small>&nbsp;&nbsp;eg. lazimpat</small>
            </label>
            <input type="text" name="street" id="street" value="<?php echo set_value('street');?>" class="form-control" style="border-radius:0px;" placeholder="Street">
            <span class="error">
              <?php echo form_error('street'); ?>
            </span>
          </div>
          <div class="form-group col-lg-5">
            <label for="fulladdress">
              Full Address :
              <small>&nbsp;&nbsp;eg. Lazimpat-2, Kathmandu, Nepal</small>
            </label>
            <input type="text" name="fulladdress" id="fulladdress" value="<?php echo set_value('fulladdress');?>" class="form-control" style="border-radius:0px;" placeholder="Full Address">
            <span class="error">
              <?php echo form_error('fulladdress'); ?>
            </span>
          </div>
        </div>
        <!--street and full address end here-->      
        <!--mobile no. and other no. start-->      
        <div class="row">
          <div class="form-group col-lg-2">
            <label for="postcode">
              Post Code:
              <small>&nbsp;&nbsp;eg. 977</small>
            </label>
            <input type="number" name="postcode" id="postcode" value="<?php echo set_value('postcode');?>" class="form-control" style="border-radius:0px;" placeholder="Post Code">
            <span class="error">
              <?php echo form_error('postcode'); ?>
            </span>
            </div>
          <div class="form-group col-lg-3">
            <label for="mobileno">
              Mobile No.:
              <small>&nbsp;&nbsp;eg. 980000000</small>
            </label>
            <input type="number" name="mobileno" id="mobileno" value="<?php echo set_value('mobileno');?>" class="form-control" style="border-radius:0px;" placeholder="Mobile No.">
            <span class="error">
              <?php echo form_error('mobileno'); ?>
            </span>
          </div>
          <div class="form-group col-lg-3">
            <label for="othertel">
              Other Tel No.:
              <small>&nbsp;&nbsp;eg. 014441561</small>
            </label>
            <input type="number" name="othertel" id="othertel" value="<?php echo set_value('othertel');?>" class="form-control" style="border-radius:0px;" placeholder="Other Tel No.">
            <span class="error">
              <?php echo form_error('othertel'); ?>
            </span>
          </div>
        </div>
        <!--mobile no. and other no. end here-->      
        <!--email start-->      
        <div class="row">
          <div class="form-group col-lg-4">
            <label for="email">
              Email :
              <small>&nbsp;&nbsp;eg. gloomy.gurung@gmail.com</small>
            </label>
            <input type="text" name="email" id="email" value="<?php echo set_value('email');?>" class="form-control" style="border-radius:0px;" placeholder="Email">
            <span class="error">
              <?php echo form_error('email'); ?>
            </span>
          </div>
        </div>
        <!--email ends here-->      
        <!--username and usertype start-->      
        <div class="row">
          <div class="form-group col-lg-3">
            <label for="username">Login username:
              <small>&nbsp;&nbsp;eg. john56</small>
            </label>
            <input type="text" name="username" id="username" value="<?php echo set_value('username'); ?>" class="form-control" style="border-radius:0px;" placeholder="Login username">
            <span class="error">
              <?php echo form_error('username'); ?>
            </span>
          </div><!--login username ends here-->
        </div>
        <!--username ends here-->
        <!--password-->
        <div class="row">
          <div class="form-group col-lg-3">
            <label for="password">
              Login Password :
              <small>&nbsp;&nbsp;eg. p@ssword</small>
            </label>
            <input type="password" name="password" id="password" value="<?php echo set_value('password'); ?>" class="form-control" style="border-radius:0px;" placeholder="**********">
            <span class="error">
              <?php echo form_error('password'); ?>
            </span>
          </div>
          <div class="form-group col-lg-3">
            <label for="repassword">
              Confirm Password :
            </label>
            <input type="password" name="repassword" id="repassword" value="<?php echo set_value('repassword'); ?>" class="form-control" style="border-radius:0px;" placeholder="***********">
            <span class="error">
              <?php echo form_error('repassword'); ?>
            </span>
          </div>
        </div><!--password ends here-->
        <!--submit and cancel button-->
        <!-- <input type="submit" value="SUBMIT"> -->
        <div class="row">
          <p>
            <button type="submit" class="btn btn-primary" style="margin-left:1em; border-radius:0px;">Submit</button>
            <button type="reset" onclick="window.location.href='<?=site_url('users'); ?>'" class="btn btn-danger" style="margin-top:0px; border-radius:0px;">Cancel</button>
          </p><hr>
        </div>

        <!--submit and cancel button end here-->
      </form>
      <!--Form ends here--> 
