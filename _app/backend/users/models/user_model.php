<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {

	//sets validation rules for new admin
  public $validation_newAdmin = array(
            array(
              'field' => 'firstname',
              'label' => 'firstname',
              'rules' => 'required|trim|sanitize'
            ),
            array(
              'field' => 'lastname',
              'label' => 'lastname',
              'rules' => 'required|trim|sanitize'
            ),
            array(
              'field' => 'street',
              'label' => 'street',
              'rules' => 'required|trim|sanitize'
            ),
            array(
              'field' => 'fulladdress',
              'label' => 'fulladdress',
              'rules' => 'required|trim|sanitize'
            ),
            array(
              'field' => 'postcode',
              'label' => 'postcode',
              'rules' => 'required|trim|sanitize'
            ),
            array(
              'field' => 'mobileno',
              'label' => 'mobileno',
              'rules' => 'required|trim|sanitize|min_length[10]|max_length[10]'
            ),
            array(
              'field' => 'othertel',
              'label' => 'othertel',
              'rules' => 'trim|sanitize|min_length[9]'
            ),
            array(
              'field' => 'email',
              'label' => 'email',
              'rules' => 'required|trim|sanitize|valid_email|is_unique[user.email]'
            ),
            array(
              'field' => 'username',
              'label' => 'username',
              'rules' => 'required|trim|sanitize|min_length[5]|max_length[25]|is_unique[user.login_username]'
            ),
            array(
              'field' => 'password',
              'label' => 'password',
              'rules' => 'required|trim|sanitize|min_length[6]|max_length[15]'
            ),
            array(
              'field' => 'repassword',
              'label' => 'repassword',
              'rules' => 'required|trim|sanitizemin_length[6]|max_length[15]|matches[password]'
            )
  );

	/**
	 *
	 * @return 
	 */
  public function getUserById($user_id){
    $query = $this->db->get_where('user', array('user_id' => $user_id));
    return $query->result();
  }

  /**
   *
   * @return 
   */
  public function getAllUsers(){
    $query = $this->db->get('user');
    return $query->result();
  }

  /**
   *
   * @return 
   */
  public function getUserByType($user_type){
    $query = $this->db->get_where('user', array('user_type' => $user_type));
    return $query->result();
  }

  /**
   *
   * @return 
   */
  public function insertAdmin($insertData){
		$this->db->insert('user',$insertData);
  	return $this->db->insert_id();
  }

  /**
   *
   * @return 
   */
  public function updateUser($user_id, $updateData){
    $this->db->where('user_id', $user_id);
    $this->db->update('user', $updateData);
    return true;
  }

  /**
   *
   * @return 
   */
  public function deleteUser($user_id){
    $this->db->delete('user', array('user_id' => $user_id));
    return true;
  }

}

/* End of file user_model.php */
/* Location: .//C/xampp/htdocs/electrobid/_app/backend/users/models/user_model.php */