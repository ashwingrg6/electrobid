<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends MX_Controller {

	/******************** Constructor Function************************/
	public function __construct()
	{
		parent::__construct();
		// Modules::run('users/checkUsertype');
		$this->checkLoginSession();
		// $this->checkUsertype();
		$this->load->model('user_model','user');
	}
	/**********************Constructor Ends**************************/

	public function index(){
		$this->checkUsertype();
		$data['allusers'] = $this->user->getAllUsers();
		$data['userdetails'] = $this->dataforHeader();
		$this->template->load('backend/template/main','all_view',$data);
	}

	/**
	 *
	 * @return 
	 */
	public function checkLoginSession(){
		if(!$this->session->userdata('loggedInUser')){
			redirect('home');
		}
	}

	/**
	 *
	 * @return 
	 */
	public function checkUsertype(){
		if($this->session->userdata('usertype') != 'admin'){
			redirect('dashboard');
		}
	}

	/**
	 *
	 * @return
	 */
	public function logout(){
		$userdata = array(
						'userinfo' => $userFlag,
						'userid' => $userFlag[0]->user_id,
						'usertype' => $userFlag[0]->user_type,
						'loggedInUser' => TRUE
					);
		$this->session->unset_userdata($userdata);
		$this->session->set_flashdata('loggedOut', 'You have successfully terminated your session....');
		redirect('home');
	}

	/**
	 *
	 * @return 
	 */
	public function add(){
		$this->checkUsertype();
		if($_POST):
      $this->form_validation->set_rules($this->user->validation_newAdmin);
    	if($this->form_validation->run() == TRUE):
    		$firstname = $this->input->post('firstname', TRUE);
				$lastname = $this->input->post('lastname', TRUE);
				$street = $this->input->post('street', TRUE);
				$fulladdress = $this->input->post('fulladdress', TRUE);
				$postcode = $this->input->post('postcode', TRUE);
				$mobileno = $this->input->post('mobileno', TRUE);
				$otherno = $this->input->post('othertel', TRUE);
				$email = $this->input->post('email', TRUE);
				$username = $this->input->post('username', TRUE);
				$password = $this->input->post('password', TRUE);
				$hashpsw = hash("sha512",$password);
				$currentdate = $this->currentDate();
				$insertData = array(
					'first_name' => $firstname,
					'last_name' => $lastname,
					'street' => $street,
					'address' => $fulladdress,
					'postcode' => $postcode,
					'mobile_no' => $mobileno,
					'other_no' => $otherno,
					'email' => $email,
					'login_username' => $username,
					'user_type' => 'admin',
					'login_password' => $hashpsw,
					'date_of_join' => $currentdate
				);
				$insertFlag = $this->user->insertAdmin($insertData);
				$this->session->set_flashdata('adminAdded', 'New admin has been registered successfully.');
				redirect('users/add');
    	else:
        $this->form_validation->set_error_delimiters('','');
      	$user_id = $this->session->userdata('userid');
				$data['userdetails'] = $this->dataforHeader();
				$this->template->load('backend/template/main','add_view',$data);
    	endif;//validation if ends
		else:
			$data['userdetails'] = $this->dataforHeader();
			$this->template->load('backend/template/main','add_view',$data);
		endif;//posted if, ends

	}

	/**
	 *
	 * @return 
	 */
	public function filter(){
		$this->checkUsertype();
		if($_POST){
			$usertype = $this->input->post('usertype', TRUE);
			if($usertype == 'all'):
				redirect('users');
			else:
				$data['userType'] = null;
				if($usertype == 'admin'):
					$data['userType'] = 'admin';
				else:
					$data['userType'] = 'user';
				endif;
				$data['allusers'] = $this->user->getUserByType($usertype);
				$data['userdetails'] = $this->dataforHeader();
				$this->template->load('backend/template/main','all_view',$data);
			endif;
		}
		else{
			redirect('users');
		}
	}


	/**
	 *
	 * @return 
	 */
	public function delete($user_id){
		$this->checkUsertype();
		$deleteFlag = $this->user->deleteUser($user_id);
		if($deleteFlag)
			$this->session->set_flashdata('deletedUserMsg', 'Success: Selected user has been deleted 	successfully.');
			redirect('users');
	}

	/**
	 *
	 * @return 
	 */
	public function dataforHeader(){
		$user_id = $this->session->userdata('userid');
		return $this->user->getUserById($user_id);
	}

	/**
	 *
	 * @return 
	 */
  public function currentDate(){
    $time = now();
    $dateformat = "%Y-%m-%d";
    return mdate($dateformat, $time);
  }


}

/* End of file users.php */
/* Location: .//C/xampp/htdocs/electro/_app/backend/users/controllers/users.php */